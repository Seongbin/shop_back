package com.example.demo.prouctManage;

import com.demo.common.dto.product.ProductDetailDto;
import com.demo.common.dto.product.ProductDto;
import com.demo.common.dto.productManage.AddProductDto;
import com.demo.common.dto.userLogin.UserLoginDto;
import com.demo.common.form.productManage.AddProductForm;
import com.demo.common.form.productManage.ModifyProductForm;
import com.demo.common.form.productManage.ProductManageDeleteForm;
import com.demo.common.jwt.JwtComponent;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * ProductManageController
 */
@RestController
@RequiredArgsConstructor
public class ProductManageController {

    /**
     * productManageService
     */
    private final ProductManageService productManageService;

    /**
     * jwtComponent
     */
    private final JwtComponent jwtComponent;

    /**
     * getProductManage
     *
     * @param jwt jwt
     * @return List<ProductDto>
     */
    @GetMapping("/productManages")
    public List<ProductDto> getProductManage(@RequestHeader(value = "Authorization") String jwt) {
        UserLoginDto userLoginDto = jwtComponent.getUserFromJwt(jwt);
        List<ProductDto> productDtos = productManageService.getProductManageListById(userLoginDto.getUserId());
        return productDtos;
    }

    /**
     * addProductManage
     *
     * @param addProductForm addProductForm
     * @param jwt            jwt
     * @return productId
     */
    @PostMapping("/addProduct")
    public int addProductManage(
            @RequestBody AddProductForm addProductForm,
            @RequestHeader(value = "Authorization") String jwt) {
        ModelMapper modelMapper = new ModelMapper();
        AddProductDto addProductDto = new AddProductDto();
        modelMapper.map(addProductForm, addProductDto);
        UserLoginDto userLoginDto = jwtComponent.getUserFromJwt(jwt);
        int prouctId = productManageService.addProduct(addProductDto, userLoginDto.getUserId());
        return prouctId;
    }

    /**
     * getModifyProduct
     *
     * @param jwt       jwt
     * @param productId productId
     * @return ProductDetailDto
     */
    @GetMapping("/modifyProduct/{productId}")
    public ProductDetailDto getModifyProduct(
            @RequestHeader(value = "Authorization") String jwt,
            @PathVariable(name = "productId") int productId) {
        jwtComponent.getUserFromJwt(jwt);
        //check

        ProductDetailDto productDetailDto = productManageService.getProductInfoByProductId(productId);
        return productDetailDto;
    }

    /**
     * modifyProductInfo
     *
     * @param jwt jwt
     * @return ProductDetailDto
     */
    @PostMapping("/modifyProduct")
    public ProductDetailDto modifyProductInfo(
            @RequestBody ModifyProductForm modifyProductForm,
            @RequestHeader(value = "Authorization") String jwt) {
        UserLoginDto userLoginDto = jwtComponent.getUserFromJwt(jwt);

        ProductDetailDto productDetailDto = new ProductDetailDto();
        ProductDto productDto = new ProductDto();
        productDto.setId(modifyProductForm.getId());
        productDto.setPrice(modifyProductForm.getPrice());
        productDto.setUserId(userLoginDto.getUserId());
        productDto.setName(modifyProductForm.getName());

        productDetailDto.setProductDto(productDto);
        productDetailDto.setProductDescriptions(modifyProductForm.getProductDescriptions());
        productDetailDto.setProductOptions(modifyProductForm.getProductOptions());
        productDetailDto = productManageService.modify(productDetailDto);
        return productDetailDto;
    }

    /**
     * deleteProduct
     *
     * @param productManageDeleteForm productManageDeleteForm
     * @param jwt                     jwt
     * @return delete count
     */
    @PostMapping("/deleteProduct")
    public int deleteProduct(
            @RequestBody ProductManageDeleteForm productManageDeleteForm,
            @RequestHeader(value = "Authorization") String jwt) {
        jwtComponent.getUserFromJwt(jwt);
        int deleteCount = productManageService.deleteProduct(productManageDeleteForm.getId());
        return deleteCount;
    }
}
