package com.example.demo.orderManage;

import com.demo.common.dto.orderManage.OrderStateOrderDto;
import com.demo.common.dto.userLogin.UserLoginDto;
import com.demo.common.form.orderManage.OrderStateOrderForm;
import com.demo.common.jwt.JwtComponent;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * OrderManageController
 */
@RestController
@RequiredArgsConstructor
public class OrderManageController {

    /**
     * orderManageService
     */
    private final OrderManageService orderManageService;

    /**
     * jwtComponent
     */
    private final JwtComponent jwtComponent;

    /**
     * getOrderManages
     *
     * @param jwt jwt
     * @return List<OrderStateOrder>
     */
    @GetMapping("/orderManages")
    public List<OrderStateOrderDto> getOrderManages(
            @RequestHeader(name = "Authorization") String jwt) {
        UserLoginDto userLoginDto = jwtComponent.getUserFromJwt(jwt);
        List<OrderStateOrderDto> orderStateOrderList = orderManageService.getOrderManage(userLoginDto.getUserId());
        return orderStateOrderList;
    }

    /**
     * orderState
     *
     * @param orderStateOrderForm orderStateOrderForm
     * @param jwt                 jwt
     */
    @PostMapping("/orderState")
    public int orderState(
            @RequestBody OrderStateOrderForm orderStateOrderForm,
            @RequestHeader(name = "Authorization") String jwt
    ) {
        int updateCount = orderManageService.setOrderManage(orderStateOrderForm.getId(), orderStateOrderForm.getOrderState());
        return updateCount;
    }
}
