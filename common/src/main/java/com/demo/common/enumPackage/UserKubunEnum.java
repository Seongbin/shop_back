package com.demo.common.enumPackage;

/**
 * UserKubun
 */
public enum UserKubunEnum {
    Common(1),
    Admin(2),
    ;

    private final int userKubun;

    UserKubunEnum(int userKubun) {
        this.userKubun = userKubun;
    }

    public int getUserKubun() {
        return this.userKubun;
    }

}
