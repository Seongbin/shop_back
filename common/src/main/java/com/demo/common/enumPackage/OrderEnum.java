package com.demo.common.enumPackage;

/**
 * OrderEnum
 */
public enum OrderEnum {
    OrderStart(1),
    ExportReady(2),
    Exported(3),
    Arrived(4),
    OrderCancel(5),
    ;

    private final int orderEnum;

    OrderEnum(int orderEnum) {
        this.orderEnum = orderEnum;
    }

    public int getOrderEnum() {
        return this.orderEnum;
    }
}
