package com.demo.common.dto.cart;

import lombok.Data;

/**
 * CartDto
 */
@Data
public class CartDto {
    /**
     * Column: product_id
     */
    private Integer productId;

    /**
     * Column: user_id
     */
    private Integer userId;

    /**
     * Column: count
     */
    private Integer count;

    /**
     * Column: productOptionId
     */
    private Integer productOptionId;
}
