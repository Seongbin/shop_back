package com.demo.common.dto.productManage;

import lombok.Data;

/**
 * PictureDto
 */
@Data
public class PictureDto {

    /**
     * id
     */
    private int id;

    /**
     * picturePath
     */
    private String picturePath;

    /**
     * productId
     */
    private int productId;
}
