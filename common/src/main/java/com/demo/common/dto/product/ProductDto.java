package com.demo.common.dto.product;

import lombok.Data;

/**
 * ProductDto
 */
@Data
public class ProductDto {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: name
     */
    private String name;

    /**
     * Column: price
     */
    private String price;

    /**
     * Column: user_id
     */
    private Integer userId;

    /**
     * thumbnail
     */
    private String thumbnail;
}
