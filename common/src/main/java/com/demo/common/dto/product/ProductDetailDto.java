package com.demo.common.dto.product;

import com.demo.common.dto.qna.QnaboardDto;
import lombok.Data;

import java.util.List;

/**
 * ProductDetailDto
 */
@Data
public class ProductDetailDto {

    /**
     * ProductDto
     */
    private ProductDto productDto;

    /**
     * List<ProductOption>
     */
    private List<ProductOptionDto> productOptions;

    /**
     * List<ProductPicture>
     */
    private List<ProductPictureDto> productPictures;

    /**
     * List<ProductDescription>
     */
    private List<ProductDescriptionDto> productDescriptions;

    /**
     * List<Qnaboard>
     */
    private List<QnaboardDto> qnaBoards;
}
