package com.demo.common.dto.qnaReply;

import lombok.Data;

/**
 * QnaReplyDto
 */
@Data
public class QnaReplyDto {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: content
     */
    private String content;

    /**
     * Column: QnABoard_id
     */
    private Integer qnaboardId;

    /**
     * Column: user_id
     */
    private Integer userId;
}
