package com.demo.common.dto.product;

import lombok.Data;

@Data
public class ProductPictureDto {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: picture_path
     */
    private String picturePath;

    /**
     * Column: product_id
     */
    private Integer productId;
}
