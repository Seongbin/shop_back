package com.demo.common.dto.userLogin;

import lombok.Data;

/**
 * UserLoginDto
 */
@Data
public class UserLoginDto {

    /**
     * userId
     */
    private Integer userId;

    /**
     * email
     */
    private String email;

    /**
     * password
     */
    private String password;
}
