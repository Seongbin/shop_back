package com.demo.common.dto.orderManage;

import com.demo.common.enumPackage.OrderEnum;
import lombok.Data;

import java.util.List;

@Data
public class OrderStateOrderDto {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: product_id
     */
    private Integer productId;

    /**
     * Column: productName
     */
    private String productName;

    /**
     * Column: user_id
     */
    private Integer userId;

    /**
     * Column: orderState
     */
    private String orderState;

    /**
     * orderStates
     */
    private List<OrderEnum> orderStates;
}
