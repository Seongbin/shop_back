package com.demo.common.dto.productManage;

import lombok.Data;

import java.util.List;

/**
 * AddProductDto
 */
@Data
public class AddProductDto {

    /**
     * Column: name
     */
    private String name;

    /**
     * Column: price
     */
    private Integer price;

    /**
     * Column: thumbnail
     */
    private String thumbnail;

    /**
     * Column: option
     */
    private List<String> option;

    /**
     * Column: picture_path
     */
    private List<String> picturePath;

    /**
     * Column: description
     */
    private List<String> description;
}
