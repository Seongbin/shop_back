package com.demo.common.dto.order;

import lombok.Data;

@Data
public class OrderDto {
    /**
     * Column: product_id
     */
    private Integer productId;

    /**
     * Column: user_id
     */
    private Integer userId;

    /**
     * Column: count
     */
    private Integer count;

    /**
     * Column: productOptionId
     */
    private Integer productOptionId;
}