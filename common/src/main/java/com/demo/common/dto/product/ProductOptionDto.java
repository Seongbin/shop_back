package com.demo.common.dto.product;

import lombok.Data;

@Data
public class ProductOptionDto {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: option
     */
    private String option;

    /**
     * Column: product_id
     */
    private Integer productId;
}
