package com.demo.common.dto.userLogin;

import lombok.Data;

@Data
public class UserKubunDto {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: kubun
     */
    private Integer kubun;

    /**
     * Column: kubun_name
     */
    private String kubunName;
}
