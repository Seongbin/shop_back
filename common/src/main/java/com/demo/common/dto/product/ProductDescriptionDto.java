package com.demo.common.dto.product;

import lombok.Data;

@Data
public class ProductDescriptionDto {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: description
     */
    private String description;

    /**
     * Column: product_id
     */
    private Integer productId;
}
