package com.demo.common.dto.userLogin;

import lombok.Data;

/**
 * UserRegisterDto
 */
@Data
public class UserRegisterDto {

    /**
     * email
     */
    private String email;

    /**
     * username
     */
    private String username;

    /**
     * password
     */
    private String password;

    /**
     * phoneNumber
     */
    private String phoneNumber;

    /**
     * address
     */
    private String address;

    /**
     * userKubunId
     */
    private int userKubunId;
}
