package com.demo.common.dto.userLogin;

import lombok.Data;

import java.util.Date;

/**
 * UserDto
 */
@Data
public class UserDto {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: username
     */
    private String username;

    /**
     * Column: email
     */
    private String email;

    /**
     * Column: phone_number
     */
    private String phoneNumber;

    /**
     * Column: created
     */
    private Date created;

    /**
     * Column: updated
     */
    private Date updated;

    /**
     * Column: User_kubun_id
     */
    private Integer userKubunId;

    /**
     * Column: address
     */
    private String address;

    /**
     * password
     */
    private String password;

}