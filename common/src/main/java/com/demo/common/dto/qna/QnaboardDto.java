package com.demo.common.dto.qna;

import lombok.Data;

/**
 * QnaboardDto
 */
@Data
public class QnaboardDto {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: subject
     */
    private String subject;

    /**
     * Column: content
     */
    private String content;

    /**
     * Column: user_id
     */
    private Integer userId;

    /**
     * Column: product_id
     */
    private Integer productId;
}
