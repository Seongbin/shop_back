package com.demo.common.dto.cart;

import lombok.Data;

/**
 * CartsDto
 */
@Data
public class CartsDto {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: product_id
     */
    private Integer productId;

    /**
     * Column: count
     */
    private Integer count;

    /**
     * Column: name
     */
    private String name;

    /**
     * Column: price
     */
    private Integer price;

    /**
     * Column: thumbnail
     */
    private String thumbnail;

    /**
     * Column: productOption
     */
    private String productOption;

    /**
     * Column: productOptionId
     */
    private Integer productOptionId;
}
