package com.demo.common.jwt;

import com.demo.common.dto.userLogin.UserLoginDto;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * JwtComponent
 */
@Data
@Component
public class JwtComponent {

    /**
     * header
     */
    @Value("${jwt.header}")
    private String header;

    /**
     * audience
     */
    @Value("${jwt.audience}")
    private String audience;

    /**
     * subject
     */
    @Value("${jwt.subject}")
    private String subject;

    /**
     * key
     */
    @Value("${jwt.key}")
    private String key;

    /**
     * bearer
     */
    @Value("${jwt.bearer}")
    private String bearer;

    /**
     * claim email
     */
    @Value("${jwt.claim.email}")
    private String claimEmail;

    /**
     * claim userId
     */
    @Value("${jwt.claim.user-id}")
    private String claimUserId;

    /**
     * userIdでJWT生成
     *
     * @param email ユーザID
     * @return JWT
     */
    public String jwtGenerate(int userId, String email) {

        Instant now = Instant.now();
        return Jwts.builder()
                .setSubject(subject)
                .setAudience(audience)
                .claim(claimUserId, userId)
                .claim(claimEmail, email)
                .setIssuedAt(Date.from(now))
                .setExpiration(Date.from(now.plus(30, ChronoUnit.MINUTES)))
                .signWith(SignatureAlgorithm.HS256, key.getBytes()) // HS256과 Key로 Sign
                .compact();
    }

    /**
     * jwt 検証
     *
     * @param jwtBearer 　Bearer付きJWT
     * @return userId ユーザID
     */
    public UserLoginDto getUserFromJwt(String jwtBearer) {
        String jwt = jwtBearer.substring(7);
        UserLoginDto result = new UserLoginDto();
        try {
            Claims claims = Jwts.parser()
                    .requireAudience(audience)
                    .requireSubject(subject)
                    .setSigningKey(key.getBytes(StandardCharsets.UTF_8)) // Set Key
                    .parseClaimsJws(jwt)
                    .getBody();

            String email = claims.get(claimEmail, String.class);
            int userId = claims.get(claimUserId, Integer.class);
            result.setEmail(email);
            result.setUserId(userId);
        } catch (Exception e) { // 토큰이 만료되었을 경우
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Login failed");
        }// 그외 에러났을 경우
        return result;
    }
}
