package com.demo.common.form.qnaReply;

import lombok.Data;

/**
 * QnaReplyForm
 */
@Data
public class QnaReplyForm {

    /**
     * Column: content
     */
    private String content;

    /**
     * Column: QnABoard_id
     */
    private String qnaBoardId;
}
