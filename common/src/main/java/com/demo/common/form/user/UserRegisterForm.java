package com.demo.common.form.user;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * UserRegisterForm
 */
@Data
public class UserRegisterForm {

    /**
     * email
     */
    @NotEmpty(message = "Emailを入力してください")
    private String email;

    /**
     * password
     */
    @NotEmpty(message = "パスワードを入力してください")
    private String password;

    /**
     * username
     */
    @NotEmpty(message = "名前を入力してください")
    private String username;

    /**
     * phoneNumber
     */
    @NotEmpty(message = "電話番号を入力してください")
    private String phoneNumber;

    /**
     * address
     */
    @NotEmpty(message = "住所を入力してください")
    private String address;

    /**
     * userKubunId
     */
    private int userKubunId;
}
