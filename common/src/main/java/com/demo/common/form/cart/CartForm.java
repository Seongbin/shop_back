package com.demo.common.form.cart;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * CartForm
 */
@Data
public class CartForm {

    /**
     * Column: product_id
     */
    @NotNull(message = "Product Idを入力してください")
    private Integer productId;

    /**
     * Column: count
     */
    @NotNull(message = "Count を入力してください")
    private Integer count;

    /**
     * Column: product option id
     */
    @NotNull(message = "Option を入力してください")
    private Integer productOptionId;
}
