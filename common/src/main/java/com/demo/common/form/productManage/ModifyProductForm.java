package com.demo.common.form.productManage;

import com.demo.common.dto.product.ProductDescriptionDto;
import com.demo.common.dto.product.ProductOptionDto;
import com.demo.common.dto.product.ProductPictureDto;
import com.demo.common.dto.qna.QnaboardDto;
import lombok.Data;

import java.util.List;

/**
 * ModifyProductForm
 */
@Data
public class ModifyProductForm {
    /**
     * id
     */
    private int id;

    /**
     * Column: name
     */
    private String name;

    /**
     * Column: price
     */
    private String price;

    /**
     * List<ProductOption>
     */
    private List<ProductOptionDto> productOptions;

    /**
     * List<ProductPicture>
     */
    private List<ProductPictureDto> productPictures;

    /**
     * List<ProductDescription>
     */
    private List<ProductDescriptionDto> productDescriptions;

    /**
     * List<Qnaboard>
     */
    private List<QnaboardDto> qnaBoards;
}
