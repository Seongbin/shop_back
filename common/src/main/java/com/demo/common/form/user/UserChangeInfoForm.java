package com.demo.common.form.user;

import lombok.Data;

/**
 * UserChangeInfoForm
 */
@Data
public class UserChangeInfoForm {

    /**
     * username
     */
    private String username;

    /**
     * password
     */
    private String password;

    /**
     * phoneNumber
     */
    private String phoneNumber;

    /**
     * address
     */
    private String address;
}
