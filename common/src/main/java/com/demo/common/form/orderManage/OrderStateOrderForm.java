package com.demo.common.form.orderManage;

import lombok.Data;

/**
 * OrderStateOrderForm
 */
@Data
public class OrderStateOrderForm {

    /**
     * id
     */
    private int id;

    /**
     * orderState
     */
    private String orderState;
}
