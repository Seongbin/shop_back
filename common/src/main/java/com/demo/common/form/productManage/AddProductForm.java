package com.demo.common.form.productManage;

import lombok.Data;

import java.util.List;

/**
 * AddProductForm
 */
@Data
public class AddProductForm {

    /**
     * Column: name
     */
    private String name;

    /**
     * Column: price
     */
    private Integer price;

    /**
     * Column: option
     */
    private List<String> option;

    /**
     * Column: picture_path
     */
    private List<String> picturePath;

    /**
     * Column: description
     */
    private List<String> description;
}
