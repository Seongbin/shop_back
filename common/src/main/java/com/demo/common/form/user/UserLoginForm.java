package com.demo.common.form.user;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * UserLoginForm
 */
@Data
public class UserLoginForm {

    /**
     * email
     */
    @NotEmpty(message = "Emailを入力してください")
    private String email;

    /**
     * password
     */
    @NotEmpty(message = "パスワードを入力してください")
    private String password;
}
