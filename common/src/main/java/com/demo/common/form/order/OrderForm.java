package com.demo.common.form.order;

import com.demo.common.dto.order.OrderDto;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class OrderForm {

    /**
     * orderDto
     */
    @NotNull(message = "Order を入力してください")
    private List<OrderDto> ordersDto;

    /**
     * Column: product option id
     */
    @NotNull(message = "住所を入力してください")
    private String address;
}
