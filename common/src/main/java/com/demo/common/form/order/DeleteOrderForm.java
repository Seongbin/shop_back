package com.demo.common.form.order;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;

@Data
public class DeleteOrderForm {

    @NotNull(message="order idを入力してください")
    ArrayList<Integer> orderIds;
}
