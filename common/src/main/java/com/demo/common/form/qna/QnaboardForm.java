package com.demo.common.form.qna;

import lombok.Data;

/**
 * QnaboardForm
 */
@Data
public class QnaboardForm {

    /**
     * Column: subject
     */
    private String subject;

    /**
     * Column: content
     */
    private String content;

    /**
     * Column: product_id
     */
    private Integer productId;
}
