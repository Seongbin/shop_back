package com.demo.common.form.productManage;

import com.demo.common.dto.productManage.PictureDto;
import lombok.Data;

import java.util.List;

/**
 * ProductManageDeleteForm
 */
@Data
public class ProductManageDeleteForm {

    /**
     * id
     */
    private int id;

    /**
     * pictureDtoList
     */
    private List<PictureDto> pictureDtoList;
}
