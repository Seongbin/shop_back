package com.demo.common.form.cart;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;

/**
 * DeleteCartForm
 */
@Data
public class DeleteCartForm {

    /**
     * cartIds
     */
    @NotNull(message = "cart idを入力してください")
    ArrayList<Integer> cartIds;
}
