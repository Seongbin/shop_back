package com.example.demo.orderManage;

import com.demo.common.dto.orderManage.OrderStateOrderDto;

import java.util.List;

/**
 * OrderManageService
 */
public interface OrderManageService {

    /**
     * getOrderManage
     *
     * @param userId userId
     * @return List<OrderStateOrder>
     */
    List<OrderStateOrderDto> getOrderManage(int userId);

    /**
     * setOrderManage
     *
     * @param orderId    orderId
     * @param orderState orderState
     * @return update count
     */
    int setOrderManage(int orderId, String orderState);
}
