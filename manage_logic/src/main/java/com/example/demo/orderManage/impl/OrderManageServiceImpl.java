package com.example.demo.orderManage.impl;

import com.demo.common.dto.orderManage.OrderStateOrderDto;
import com.demo.common.enumPackage.OrderEnum;
import com.example.demo.mapper.OrderMapper;
import com.example.demo.mapper.OrderStateCustomMapper;
import com.example.demo.model.Order;
import com.example.demo.model.OrderStateOrder;
import com.example.demo.orderManage.OrderManageService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * OrderManageServiceImpl
 */
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class OrderManageServiceImpl implements OrderManageService {

    /**
     * orderStateCustomMapper
     */
    private final OrderStateCustomMapper orderStateCustomMapper;

    /**
     * orderMapper
     */
    private final OrderMapper orderMapper;

    @Override
    public List<OrderStateOrderDto> getOrderManage(int userId) {
        List<OrderStateOrder> orderStateOrders = orderStateCustomMapper.getOrderStateByUserId(userId);
        ModelMapper modelMapper = new ModelMapper();
        List<OrderStateOrderDto> result = new ArrayList<>();

        orderStateOrders.forEach(obj -> {
            OrderStateOrderDto orderStateOrderDto = new OrderStateOrderDto();
            modelMapper.map(obj, orderStateOrderDto);
            result.add(orderStateOrderDto);
        });
        return result;
    }

    @Transactional(readOnly = false)
    @Override
    public int setOrderManage(int orderId, String orderState) {
        Order order = new Order();
        order.setId(orderId);
        order.setOrderStateId(OrderEnum.valueOf(orderState).getOrderEnum());
        int updateCount = orderMapper.updateByPrimaryKeySelective(order);
        return updateCount;
    }
}
