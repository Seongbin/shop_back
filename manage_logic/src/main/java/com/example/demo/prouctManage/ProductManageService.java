package com.example.demo.prouctManage;

import com.demo.common.dto.product.ProductDetailDto;
import com.demo.common.dto.product.ProductDto;
import com.demo.common.dto.productManage.AddProductDto;

import java.util.List;

/**
 * ProductManageService
 */
public interface ProductManageService {

    /**
     * getProductManageListById
     *
     * @param userId userId
     * @return List<ProductDto>
     */
    List<ProductDto> getProductManageListById(int userId);

    /**
     * getProductInfoByProductId
     *
     * @param productId productId
     * @return ProductDetailDto ProductDetailDto
     */
    ProductDetailDto getProductInfoByProductId(int productId);

    /**
     * addProduct
     *
     * @param addProductDto addProductDto
     * @param userId        userId
     * @return productId
     */
    int addProduct(AddProductDto addProductDto, int userId);

    /**
     * 情報修正
     *
     * @param productDetailDto productDetailDto
     * @return ProductDetailDto
     */
    ProductDetailDto modify(ProductDetailDto productDetailDto);

    /**
     * deleteProduct
     *
     * @param productId productId
     * @return delete count
     */
    int deleteProduct(int productId);
}
