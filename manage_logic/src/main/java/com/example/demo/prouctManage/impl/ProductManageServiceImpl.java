package com.example.demo.prouctManage.impl;

import com.demo.common.dto.product.*;
import com.demo.common.dto.productManage.AddProductDto;
import com.example.demo.mapper.ProductDescriptionMapper;
import com.example.demo.mapper.ProductMapper;
import com.example.demo.mapper.ProductOptionMapper;
import com.example.demo.mapper.ProductPictureMapper;
import com.example.demo.model.*;
import com.example.demo.prouctManage.ProductManageService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * ProductManageServiceImpl
 */
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class ProductManageServiceImpl implements ProductManageService {
    /**
     * thumbnailPath
     */
    @Value("${file-path.thumbnail}")
    private String thumbnailPath;

    /**
     * photoPath
     */
    @Value("${file-path.photo}")
    private String photoPath;

    /**
     * productMapper
     */
    private final ProductMapper productMapper;

    /**
     * productOptionMapper
     */
    private final ProductOptionMapper productOptionMapper;

    /**
     * productDescriptionMapper
     */
    private final ProductDescriptionMapper productDescriptionMapper;

    /**
     * productPictureMapper
     */
    private final ProductPictureMapper productPictureMapper;

    @Override
    public List<ProductDto> getProductManageListById(int userId) {
        ProductExample productExample = new ProductExample();
        productExample.createCriteria().andUserIdEqualTo(userId);
        List<Product> products = productMapper.selectByExample(productExample);
        List<ProductDto> resultProducts = new ArrayList<>();

        ModelMapper modelMapper = new ModelMapper();
        products.forEach(obj -> {
            ProductDto productDto = new ProductDto();
            modelMapper.map(obj, productDto);
            resultProducts.add(productDto);
        });
        return resultProducts;
    }

    @Override
    public ProductDetailDto getProductInfoByProductId(int productId) {
        ModelMapper modelMapper = new ModelMapper();
        Product product = productMapper.selectByPrimaryKey(productId);
        ProductDto productDto = new ProductDto();
        productDto.setPrice(product.getPrice().toString());
        productDto.setName(product.getName());
        productDto.setPrice(product.getPrice().toString());
        productDto.setId(productId);
        productDto.setThumbnail(product.getThumbnail());

        ProductDetailDto productDetailDto = new ProductDetailDto();
        productDetailDto.setProductDto(productDto);

        ProductDescriptionExample productDescriptionExample = new ProductDescriptionExample();
        productDescriptionExample.createCriteria().andProductIdEqualTo(productId);
        List<ProductDescription> productDescriptionList = productDescriptionMapper.selectByExample(productDescriptionExample);

        List<ProductDescriptionDto> productDescriptions = new ArrayList<>();
        productDescriptionList.forEach(obj -> {
            ProductDescriptionDto productDescriptionDto = new ProductDescriptionDto();
            modelMapper.map(obj, productDescriptionDto);
            productDescriptions.add(productDescriptionDto);
        });
        productDetailDto.setProductDescriptions(productDescriptions);

        ProductOptionExample productOptionExample = new ProductOptionExample();
        productOptionExample.createCriteria().andProductIdEqualTo(productId);
        List<ProductOption> productOptionList = productOptionMapper.selectByExample(productOptionExample);
        List<ProductOptionDto> productOptions = new ArrayList<>();
        productOptionList.forEach(obj -> {
            ProductOptionDto productOptionDto = new ProductOptionDto();
            modelMapper.map(obj, productOptionDto);
            productOptions.add(productOptionDto);
        });
        productDetailDto.setProductOptions(productOptions);

        ProductPictureExample productPictureExample = new ProductPictureExample();
        productPictureExample.createCriteria().andProductIdEqualTo(productId);
        List<ProductPicture> productPictureList = productPictureMapper.selectByExample(productPictureExample);

        List<ProductPictureDto> productPictures = new ArrayList<>();
        productPictureList.forEach(obj -> {
            ProductPictureDto productPictureDto = new ProductPictureDto();
            modelMapper.map(obj, productPictureDto);
            productPictures.add(productPictureDto);
        });
        productDetailDto.setProductPictures(productPictures);
        return productDetailDto;
    }

    @Override
    @Transactional(readOnly = false)
    public int addProduct(AddProductDto addProductDto, int userId) {

        Product product = new Product();

        product.setPrice(addProductDto.getPrice());
        product.setName(addProductDto.getName());
        product.setUserId(userId);
        product.setThumbnail(addProductDto.getThumbnail());
        int insertedCount = productMapper.insert(product);

        addProductDto.getOption().forEach(obj -> {
            ProductOption productOption = new ProductOption();
            productOption.setOption(obj);
            productOption.setProductId(product.getId());
            productOptionMapper.insert(productOption);
        });

        addProductDto.getDescription().forEach(obj -> {
            ProductDescription productDescription = new ProductDescription();
            productDescription.setDescription(obj);
            productDescription.setProductId(product.getId());
            productDescriptionMapper.insert(productDescription);
        });
        return product.getId();
    }

    @Override
    @Transactional(readOnly = false)
    public ProductDetailDto modify(ProductDetailDto productDetailDto) {

        ProductOptionExample productOptionExample = new ProductOptionExample();
        ProductPictureExample productPictureExample = new ProductPictureExample();
        ProductDescriptionExample productDescriptionExample = new ProductDescriptionExample();
        int productId = productDetailDto.getProductDto().getId();

        productOptionExample.createCriteria().andProductIdEqualTo(productId);
        productPictureExample.createCriteria().andProductIdEqualTo(productId);
        productDescriptionExample.createCriteria().andProductIdEqualTo(productId);

        productOptionMapper.deleteByExample(productOptionExample);
        productDescriptionMapper.deleteByExample(productDescriptionExample);
        productPictureMapper.deleteByExample(productPictureExample);

        productDetailDto.getProductOptions().forEach(obj -> {
            ProductOption productOption = new ProductOption();
            productOption.setOption(obj.getOption());
            productOption.setProductId(productId);
            productOptionMapper.insert(productOption);
        });

        productDetailDto.getProductDescriptions().forEach(obj -> {
            ProductDescription productDescription = new ProductDescription();
            productDescription.setDescription(obj.getDescription());
            productDescription.setProductId(productId);
            productDescriptionMapper.insert(productDescription);
        });

        return productDetailDto;
    }

    @Override
    @Transactional(readOnly = false)
    public int deleteProduct(int productId) {
        ProductOptionExample productOptionExample = new ProductOptionExample();
        productOptionExample.createCriteria().andProductIdEqualTo(productId);

        ProductDescriptionExample productDescriptionExample = new ProductDescriptionExample();
        productDescriptionExample.createCriteria().andProductIdEqualTo(productId);

        ProductPictureExample productPictureExample = new ProductPictureExample();
        productPictureExample.createCriteria().andProductIdEqualTo(productId);

        Product product = productMapper.selectByPrimaryKey(productId);
        List<ProductPicture> productPictures = productPictureMapper.selectByExample(productPictureExample);
        productPictures.forEach(obj -> {
            File file = new File(photoPath + obj.getPicturePath());
            file.delete();
        });

        File file = new File(thumbnailPath + product.getThumbnail());
        file.delete();

        productDescriptionMapper.deleteByExample(productDescriptionExample);
        productOptionMapper.deleteByExample(productOptionExample);
        productPictureMapper.deleteByExample(productPictureExample);
        int deleteCount = productMapper.deleteByPrimaryKey(productId);
        return deleteCount;
    }
}
