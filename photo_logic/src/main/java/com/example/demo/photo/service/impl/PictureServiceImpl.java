package com.example.demo.photo.service.impl;

import com.example.demo.mapper.ProductPictureMapper;
import com.example.demo.model.ProductPicture;
import com.example.demo.model.ProductPictureExample;
import com.example.demo.photo.service.PictureService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * PictureServiceImpl
 */
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class PictureServiceImpl implements PictureService {

    /**
     * productPictureMapper
     */
    private final ProductPictureMapper productPictureMapper;

    @Override
    public List<String> getPictures(int productId) {
        ProductPictureExample productPictureExample = new ProductPictureExample();
        productPictureExample.createCriteria().andProductIdEqualTo(productId);
        List<ProductPicture> productPictureList = productPictureMapper.selectByExample(productPictureExample);
        List<String> pictureList = new ArrayList<>();
        productPictureList.forEach(obj -> {
            pictureList.add(obj.getPicturePath());
        });
        return pictureList;
    }
}
