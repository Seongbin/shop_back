package com.example.demo.photo.service.impl;

import com.demo.common.dto.productManage.PictureDto;
import com.example.demo.mapper.ProductMapper;
import com.example.demo.mapper.ProductPictureMapper;
import com.example.demo.model.Product;
import com.example.demo.model.ProductExample;
import com.example.demo.model.ProductPicture;
import com.example.demo.model.ProductPictureExample;
import com.example.demo.photo.service.PictureManageService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * PictureManageServiceImpl
 */
@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class PictureManageServiceImpl implements PictureManageService {

    /**
     * thumbnailPath
     */
    @Value("${file-path.thumbnail}")
    private String thumbnailPath;

    /**
     * photoPath
     */
    @Value("${file-path.photo}")
    private String photoPath;

    /**
     * productMapper
     */
    private final ProductMapper productMapper;

    /**
     * productPictureMapper
     */
    private final ProductPictureMapper productPictureMapper;

    @Override
    @Transactional(readOnly = false)
    public int insertThumbnail(int productId, MultipartFile multipartFile) throws Exception {
        if (multipartFile.isEmpty()) {
            return -1;
        }
        // jpeg, png, gif 파일들만 받아서 처리할 예정
        String contentType = multipartFile.getContentType();
        String originalFileExtension;
        // 확장자 명이 없으면 이 파일은 잘 못 된 것이다
        if (ObjectUtils.isEmpty(contentType)) {
            return -1;
        } else {
            if (contentType.contains("image/jpeg")) {
                originalFileExtension = ".jpg";
            } else if (contentType.contains("image/png")) {
                originalFileExtension = ".png";
            } else if (contentType.contains("image/gif")) {
                originalFileExtension = ".gif";
            }
            // 다른 파일 명이면 아무 일 하지 않는다
            else {
                return -1;
            }
        }
        // 각 이름은 겹치면 안되므로 나노 초까지 동원하여 지정
        String newFileName = System.nanoTime() + originalFileExtension;
        // 저장된 파일로 변경하여 이를 보여주기 위함
        File file = new File(thumbnailPath + newFileName);
        multipartFile.transferTo(file);

        ProductExample productExample = new ProductExample();
        productExample.createCriteria().andIdEqualTo(productId);
        Product product = new Product();

        product.setThumbnail(newFileName);
        int updateCount = productMapper.updateByExampleSelective(product, productExample);
        return updateCount;
    }

    @Override
    @Transactional(readOnly = false)
    public int insertPhoto(int productId, List<MultipartFile> fileList) throws Exception {
        int insertCount = 0;
        for (MultipartFile multipartFile : fileList) {
            // 파일이 비어 있지 않을 때 작업을 시작해야 오류가 나지 않는다
            if (multipartFile.isEmpty()) {
                return -1;
            }

            // jpeg, png, gif 파일들만 받아서 처리할 예정
            String contentType = multipartFile.getContentType();
            String originalFileExtension;
            // 확장자 명이 없으면 이 파일은 잘 못 된 것이다
            if (ObjectUtils.isEmpty(contentType)) {
                return -1;
            } else {
                if (contentType.contains("image/jpeg")) {
                    originalFileExtension = ".jpg";
                } else if (contentType.contains("image/png")) {
                    originalFileExtension = ".png";
                } else if (contentType.contains("image/gif")) {
                    originalFileExtension = ".gif";
                }
                // 다른 파일 명이면 아무 일 하지 않는다
                else {
                    return -1;
                }
            }
            // 각 이름은 겹치면 안되므로 나노 초까지 동원하여 지정
            String newFileName = System.nanoTime() + originalFileExtension;
            // 저장된 파일로 변경하여 이를 보여주기 위함
            File file = new File(photoPath + newFileName);
            multipartFile.transferTo(file);

            ProductPicture productPicture = new ProductPicture();
            productPicture.setProductId(productId);
            productPicture.setPicturePath(newFileName);
            insertCount += productPictureMapper.insert(productPicture);
        }

        ProductPicture productPicture = new ProductPicture();
        productPicture.setProductId(productId);
        return insertCount;
    }

    @Override
    @Transactional(readOnly = false)
    public int deleteThumbnail(int productId) {
        Product product = productMapper.selectByPrimaryKey(productId);
        File file = new File(thumbnailPath + product.getThumbnail());
        if (file.exists()) {
            if (file.delete()) {
                System.out.println("success");
            } else {
                System.out.println("fail1");
            }
        } else {
            System.out.println("fail2");
        }

        product.setThumbnail("");
        int deleteCount = productMapper.updateByPrimaryKey(product);
        return deleteCount;
    }

    @Override
    @Transactional(readOnly = false)
    public int deletePhoto(List<PictureDto> pictureDtoList) {
        List<String> picturePath = new ArrayList<>();
        pictureDtoList.forEach(obj -> {
            picturePath.add(obj.getPicturePath());
            File file = new File(photoPath + obj.getPicturePath());
            if (file.exists()) {
                if (file.delete()) {
                    System.out.println("success");
                } else {
                    System.out.println("fail1");
                }
            } else {
                System.out.println("fail2");
            }

        });
        ProductPictureExample productPictureExample = new ProductPictureExample();
        productPictureExample.createCriteria().andPicturePathIn(picturePath);
        productPictureMapper.deleteByExample(productPictureExample);
        return 0;
    }
}
