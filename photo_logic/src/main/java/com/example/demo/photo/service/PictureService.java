package com.example.demo.photo.service;

import java.util.List;

/**
 * PictureService
 */
public interface PictureService {

    /**
     * getPictures
     *
     * @param productId productId
     * @return List<String>
     */
    List<String> getPictures(int productId);
}
