package com.example.demo.photo.service;

import com.demo.common.dto.productManage.PictureDto;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * PictureManageService
 */
public interface PictureManageService {

    /**
     * insertThumbnail
     *
     * @param productId     productId
     * @param multipartFile multipartFile
     * @return int
     */
    int insertThumbnail(int productId, MultipartFile multipartFile) throws Exception;

    /**
     * insertPhoto
     *
     * @param productId productId
     * @param fileList  fileList
     * @return int
     * @throws Exception Exception
     */
    int insertPhoto(int productId, List<MultipartFile> fileList) throws Exception;

    /**
     * deleteThumbnail
     *
     * @param productId productId
     * @return delete count
     */
    int deleteThumbnail(int productId);

    /**
     * deletePhoto
     *
     * @param pictureDtoList pictureDtoList
     * @return delete count
     */
    int deletePhoto(List<PictureDto> pictureDtoList);
}
