package com.example.demo.mapper;

import com.example.demo.model.OrderStateOrder;
import com.example.demo.model.Orders;

import java.util.List;

public interface OrderStateCustomMapper {

    /**
     * getOrderStateByUserId
     *
     * @param userId userId
     * @return List<OrderStateOrder>
     */
    List<OrderStateOrder> getOrderStateByUserId(int userId);
}
