package com.example.demo.mapper;

import com.example.demo.model.Cart;
import com.example.demo.model.CartExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CartMapper {
    /**
     * @mbg.generated generated automatically, do not modify!
     */
    long countByExample(CartExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int deleteByExample(CartExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int insert(Cart record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int insertSelective(Cart record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    List<Cart> selectByExample(CartExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    Cart selectByPrimaryKey(Integer id);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByExampleSelective(@Param("record") Cart record, @Param("example") CartExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByExample(@Param("record") Cart record, @Param("example") CartExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByPrimaryKeySelective(Cart record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByPrimaryKey(Cart record);

}