package com.example.demo.mapper;

import com.example.demo.model.Orders;

import java.util.List;

public interface OrderCustomMapper {
    /**
     * @param userId userId
     * @return List<Orders>
     */
    List<Orders> getOrdersByUserId(int userId);
}
