package com.example.demo.mapper;

import com.example.demo.model.OrderState;
import com.example.demo.model.OrderStateExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderStateMapper {
    /**
     * @mbg.generated generated automatically, do not modify!
     */
    long countByExample(OrderStateExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int deleteByExample(OrderStateExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int insert(OrderState record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int insertSelective(OrderState record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    List<OrderState> selectByExample(OrderStateExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    OrderState selectByPrimaryKey(Integer id);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByExampleSelective(@Param("record") OrderState record, @Param("example") OrderStateExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByExample(@Param("record") OrderState record, @Param("example") OrderStateExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByPrimaryKeySelective(OrderState record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByPrimaryKey(OrderState record);
}