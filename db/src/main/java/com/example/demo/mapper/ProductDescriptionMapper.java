package com.example.demo.mapper;

import com.example.demo.model.ProductDescription;
import com.example.demo.model.ProductDescriptionExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductDescriptionMapper {
    /**
     * @mbg.generated generated automatically, do not modify!
     */
    long countByExample(ProductDescriptionExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int deleteByExample(ProductDescriptionExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int insert(ProductDescription record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int insertSelective(ProductDescription record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    List<ProductDescription> selectByExample(ProductDescriptionExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    ProductDescription selectByPrimaryKey(Integer id);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByExampleSelective(@Param("record") ProductDescription record, @Param("example") ProductDescriptionExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByExample(@Param("record") ProductDescription record, @Param("example") ProductDescriptionExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByPrimaryKeySelective(ProductDescription record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByPrimaryKey(ProductDescription record);
}