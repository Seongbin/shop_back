package com.example.demo.mapper;

import com.example.demo.model.Qnaboard;
import com.example.demo.model.QnaboardExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface QnaboardMapper {
    /**
     * @mbg.generated generated automatically, do not modify!
     */
    long countByExample(QnaboardExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int deleteByExample(QnaboardExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int insert(Qnaboard record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int insertSelective(Qnaboard record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    List<Qnaboard> selectByExample(QnaboardExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    Qnaboard selectByPrimaryKey(Integer id);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByExampleSelective(@Param("record") Qnaboard record, @Param("example") QnaboardExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByExample(@Param("record") Qnaboard record, @Param("example") QnaboardExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByPrimaryKeySelective(Qnaboard record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByPrimaryKey(Qnaboard record);

}