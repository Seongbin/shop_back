package com.example.demo.mapper;

import com.example.demo.model.Order;
import com.example.demo.model.OrderExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderMapper {
    /**
     * @mbg.generated generated automatically, do not modify!
     */
    long countByExample(OrderExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int deleteByExample(OrderExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int insert(Order record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int insertSelective(Order record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    List<Order> selectByExample(OrderExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    Order selectByPrimaryKey(Integer id);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByExampleSelective(@Param("record") Order record, @Param("example") OrderExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByExample(@Param("record") Order record, @Param("example") OrderExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByPrimaryKeySelective(Order record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByPrimaryKey(Order record);
}