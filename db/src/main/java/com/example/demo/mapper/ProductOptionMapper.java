package com.example.demo.mapper;

import com.example.demo.model.ProductOption;
import com.example.demo.model.ProductOptionExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductOptionMapper {
    /**
     * @mbg.generated generated automatically, do not modify!
     */
    long countByExample(ProductOptionExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int deleteByExample(ProductOptionExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int insert(ProductOption record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int insertSelective(ProductOption record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    List<ProductOption> selectByExample(ProductOptionExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    ProductOption selectByPrimaryKey(Integer id);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByExampleSelective(@Param("record") ProductOption record, @Param("example") ProductOptionExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByExample(@Param("record") ProductOption record, @Param("example") ProductOptionExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByPrimaryKeySelective(ProductOption record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByPrimaryKey(ProductOption record);
}