package com.example.demo.mapper;

import com.example.demo.model.Carts;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CartCustomMapper {
    List<Carts> getCartsByUserId(@Param("user_id")int userId);
}
