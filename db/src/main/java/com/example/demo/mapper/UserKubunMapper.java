package com.example.demo.mapper;

import com.example.demo.model.UserKubun;
import com.example.demo.model.UserKubunExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserKubunMapper {
    /**
     * @mbg.generated generated automatically, do not modify!
     */
    long countByExample(UserKubunExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int deleteByExample(UserKubunExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int insert(UserKubun record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int insertSelective(UserKubun record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    List<UserKubun> selectByExample(UserKubunExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    UserKubun selectByPrimaryKey(Integer id);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByExampleSelective(@Param("record") UserKubun record, @Param("example") UserKubunExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByExample(@Param("record") UserKubun record, @Param("example") UserKubunExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByPrimaryKeySelective(UserKubun record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByPrimaryKey(UserKubun record);
}