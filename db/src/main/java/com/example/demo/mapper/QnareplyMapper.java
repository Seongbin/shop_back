package com.example.demo.mapper;

import com.example.demo.model.Qnareply;
import com.example.demo.model.QnareplyExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface QnareplyMapper {
    /**
     * @mbg.generated generated automatically, do not modify!
     */
    long countByExample(QnareplyExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int deleteByExample(QnareplyExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int insert(Qnareply record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int insertSelective(Qnareply record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    List<Qnareply> selectByExample(QnareplyExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    Qnareply selectByPrimaryKey(Integer id);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByExampleSelective(@Param("record") Qnareply record, @Param("example") QnareplyExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByExample(@Param("record") Qnareply record, @Param("example") QnareplyExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByPrimaryKeySelective(Qnareply record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByPrimaryKey(Qnareply record);
}