package com.example.demo.mapper;

import com.example.demo.model.ProductPicture;
import com.example.demo.model.ProductPictureExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductPictureMapper {
    /**
     * @mbg.generated generated automatically, do not modify!
     */
    long countByExample(ProductPictureExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int deleteByExample(ProductPictureExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int insert(ProductPicture record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int insertSelective(ProductPicture record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    List<ProductPicture> selectByExample(ProductPictureExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    ProductPicture selectByPrimaryKey(Integer id);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByExampleSelective(@Param("record") ProductPicture record, @Param("example") ProductPictureExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByExample(@Param("record") ProductPicture record, @Param("example") ProductPictureExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByPrimaryKeySelective(ProductPicture record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByPrimaryKey(ProductPicture record);
}