package com.example.demo.mapper;

import com.example.demo.model.Product;
import com.example.demo.model.ProductExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductMapper {
    /**
     * @mbg.generated generated automatically, do not modify!
     */
    long countByExample(ProductExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int deleteByExample(ProductExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int insert(Product record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int insertSelective(Product record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    List<Product> selectByExample(ProductExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    Product selectByPrimaryKey(Integer id);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByExampleSelective(@Param("record") Product record, @Param("example") ProductExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByExample(@Param("record") Product record, @Param("example") ProductExample example);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByPrimaryKeySelective(Product record);

    /**
     * @mbg.generated generated automatically, do not modify!
     */
    int updateByPrimaryKey(Product record);
}