package com.example.demo.model;

import lombok.Data;

/**
 * Table: qnaReply
 */
@Data
public class Qnareply {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: content
     */
    private String content;

    /**
     * Column: QnABoard_id
     */
    private Integer qnaboardId;

    /**
     * Column: user_id
     */
    private Integer userId;
}