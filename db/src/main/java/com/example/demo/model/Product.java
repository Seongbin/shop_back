package com.example.demo.model;

import lombok.Data;

/**
 * Table: product
 */
@Data
public class Product {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: name
     */
    private String name;

    /**
     * Column: price
     */
    private Integer price;

    /**
     * Column: thumbnail
     */
    private String thumbnail;

    /**
     * Column: user_id
     */
    private Integer userId;
}