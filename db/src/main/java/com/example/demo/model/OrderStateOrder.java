package com.example.demo.model;

import lombok.Data;

@Data
public class OrderStateOrder {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: product_id
     */
    private Integer productId;

    /**
     * Column: productName
     */
    private String productName;

    /**
     * Column: user_id
     */
    private Integer userId;

    /**
     * Column: orderState
     */
    private String orderState;
}
