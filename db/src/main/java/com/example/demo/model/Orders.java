package com.example.demo.model;

import lombok.Data;

/**
 * Table: order
 */
@Data
public class Orders {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: product_id
     */
    private Integer productId;

    /**
     * Column: user_id
     */
    private Integer userId;

    /**
     * Column: name
     */
    private String name;

    /**
     * Column: count
     */
    private Integer count;
    /**
     * Column: price
     */
    private Integer price;

    /**
     * Column: thumbnail
     */
    private String thumbnail;

    /**
     * Column: productOption
     */
    private String productOption;

    /**
     * Column: productOptionId
     */
    private Integer productOptionId;
    /**
     * address
     */
    private String address;
}