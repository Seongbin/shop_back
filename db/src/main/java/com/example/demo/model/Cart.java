package com.example.demo.model;

import lombok.Data;

/**
 * Table: cart
 */
@Data
public class Cart {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: product_id
     */
    private Integer productId;

    /**
     * Column: product_option_id
     */
    private Integer productOptionId;

    /**
     * Column: user_id
     */
    private Integer userId;

    /**
     * Column: count
     */
    private Integer count;
}