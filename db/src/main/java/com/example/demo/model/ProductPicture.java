package com.example.demo.model;

import lombok.Data;

/**
 * Table: product_picture
 */
@Data
public class ProductPicture {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: picture_path
     */
    private String picturePath;

    /**
     * Column: product_id
     */
    private Integer productId;
}