package com.example.demo.model;

import lombok.Data;

/**
 * Table: product_description
 */
@Data
public class ProductDescription {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: description
     */
    private String description;

    /**
     * Column: product_id
     */
    private Integer productId;
}