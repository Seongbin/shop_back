package com.example.demo.model;

import lombok.Data;

/**
 * Table: order_state
 */
@Data
public class OrderState {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: orderId
     */
    private Integer orderid;

    /**
     * Column: orderState
     */
    private String orderstate;
}