package com.example.demo.model;

import java.util.Date;
import lombok.Data;

/**
 * Table: user
 */
@Data
public class User {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: username
     */
    private String username;

    /**
     * Column: password
     */
    private String password;

    /**
     * Column: email
     */
    private String email;

    /**
     * Column: phone_number
     */
    private String phoneNumber;

    /**
     * Column: created
     */
    private Date created;

    /**
     * Column: updated
     */
    private Date updated;

    /**
     * Column: User_kubun_id
     */
    private Integer userKubunId;

    /**
     * Column: address
     */
    private String address;
}