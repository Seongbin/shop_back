package com.example.demo.model;

import lombok.Data;

/**
 * Table: QnABoard
 */
@Data
public class Qnaboard {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: subject
     */
    private String subject;

    /**
     * Column: content
     */
    private String content;

    /**
     * Column: user_id
     */
    private Integer userId;

    /**
     * Column: product_id
     */
    private Integer productId;
}