package com.example.demo.model;

import lombok.Data;

/**
 * Table: User_kubun
 */
@Data
public class UserKubun {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: kubun
     */
    private Integer kubun;

    /**
     * Column: kubun_name
     */
    private String kubunName;
}