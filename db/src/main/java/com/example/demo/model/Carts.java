package com.example.demo.model;

import lombok.Data;

@Data
public class Carts {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: product_id
     */
    private Integer productId;

    /**
     * Column: count
     */
    private Integer count;

    /**
     * Column: name
     */
    private String name;

    /**
     * Column: price
     */
    private Integer price;

    /**
     * Column: thumbnail
     */
    private String thumbnail;

    /**
     * Column: productOption
     */
    private String productOption;

    /**
     * Column: productOptionId
     */
    private Integer productOptionId;
}
