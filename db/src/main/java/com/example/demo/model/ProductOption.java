package com.example.demo.model;

import lombok.Data;

/**
 * Table: product_option
 */
@Data
public class ProductOption {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: option
     */
    private String option;

    /**
     * Column: product_id
     */
    private Integer productId;
}