package com.example.demo.model;

import lombok.Data;

/**
 * Table: order
 */
@Data
public class Order {
    /**
     * Column: id
     */
    private Integer id;

    /**
     * Column: product_id
     */
    private Integer productId;

    /**
     * Column: user_id
     */
    private Integer userId;

    /**
     * Column: count
     */
    private Integer count;

    /**
     * Column: product_option_id
     */
    private Integer productOptionId;

    /**
     * Column: address
     */
    private String address;

    /**
     * Column: order_state_id
     */
    private Integer orderStateId;
}