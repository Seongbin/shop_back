package com.example.demo.healthCheck;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthCheck {
    //TODO actuator
    @GetMapping("/")
    public String healthCheck(){
        return "success";
    }

}
