package com.example.demo.picture;

import com.demo.common.form.productManage.ProductManageDeleteForm;
import com.demo.common.jwt.JwtComponent;
import com.example.demo.photo.service.PictureManageService;
import com.example.demo.photo.service.PictureService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * PictureManageController
 */
@RestController
@RequiredArgsConstructor
public class PictureManageController {
    /**
     * pictureManageService
     */
    private final PictureManageService pictureManageService;

    /**
     * jwtComponent
     */
    private final JwtComponent jwtComponent;

    /**
     * pictureService
     */
    private final PictureService pictureService;

    /**
     * thumbnailPath
     */
    @Value("${file-path.thumbnail}")
    private String thumbnailPath;

    /**
     * photoPath
     */
    @Value("${file-path.photo}")
    private String photoPath;

    /**
     * getProductPicture
     *
     * @param productId productId
     * @return productPictures
     */
    @GetMapping("/productPicture/{productId}")
    public List<String> getProductPicture(
            @RequestParam(name = "productId") int productId) {
        List<String> productPictures = pictureService.getPictures(productId);
        return productPictures;
    }

    /**
     * getThumbnailPictureByFilename
     *
     * @param filename filename
     * @return thumbnail image
     * @throws IOException IOException
     */
    @GetMapping(
            value = "/getThumbnailByFilename/{filename}",
            produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody
    byte[] getThumbnailByFilename(
            @PathVariable(name = "filename") String filename) throws IOException {
        String filePath = thumbnailPath + filename;

        FileInputStream fis = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try {
            fis = new FileInputStream(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        int readCount = 0;
        byte[] buffer = new byte[1024];
        byte[] fileArray = null;

        try {
            while ((readCount = fis.read(buffer)) != -1) {
                baos.write(buffer, 0, readCount);
            }
            fileArray = baos.toByteArray();
            fis.close();
            baos.close();
        } catch (IOException e) {
            throw new RuntimeException("File Error");
        }
        return fileArray;
    }

    /**
     * getPictureByFilename
     *
     * @param filename filename
     * @return thumbnail image
     * @throws IOException IOException
     */
    @GetMapping(value = "/getPictureByFilename/{filename}",
            produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody
    byte[] getPictureByFilename
    (@PathVariable(name = "filename") String filename) throws IOException {
        String filePath = photoPath + filename;

        FileInputStream fis = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try {
            fis = new FileInputStream(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        int readCount = 0;
        byte[] buffer = new byte[1024];
        byte[] fileArray = null;

        try {
            while ((readCount = fis.read(buffer)) != -1) {
                baos.write(buffer, 0, readCount);
            }
            fileArray = baos.toByteArray();
            fis.close();
            baos.close();
        } catch (IOException e) {
            throw new RuntimeException("File Error");
        }
        return fileArray;
    }

    /**
     * addPicture
     *
     * @param id       id
     * @param fileList fileList
     * @param jwt      jwt
     * @return update count
     * @throws Exception Exception
     */
    @PostMapping("/addPicture")
    public int addPicture(
            @RequestParam("id") int id,
            @RequestParam("fileList") List<MultipartFile> fileList,
            @RequestHeader(value = "Authorization") String jwt) throws Exception {
        jwtComponent.getUserFromJwt(jwt);
        int updateCount = pictureManageService.insertPhoto(id, fileList);
        return updateCount;
    }

    /**
     * addThumbnail
     *
     * @param id   id
     * @param file file
     * @param jwt  jwt
     * @return insert count
     * @throws Exception Exception
     */
    @PostMapping("/addThumbnail")
    public int addThumbnail(
            @RequestParam("id") int id,
            @RequestParam("file") MultipartFile file,
            @RequestHeader(value = "Authorization") String jwt) throws Exception {
        jwtComponent.getUserFromJwt(jwt);
        int insertCount = pictureManageService.insertThumbnail(id, file);
        return insertCount;
    }

    /**
     * deletePicture
     *
     * @param productManageDeleteForm productManageDeleteForm
     * @param jwt                     jwt
     * @return delete count
     */
    @PostMapping("/deletePicture")
    public int deletePicture(
            @RequestBody ProductManageDeleteForm productManageDeleteForm,
            @RequestHeader(value = "Authorization") String jwt) {
        jwtComponent.getUserFromJwt(jwt);
        pictureManageService.deletePhoto(productManageDeleteForm.getPictureDtoList());
        return 0;
    }

    /**
     * deleteThumbnail
     *
     * @param productManageDeleteForm productManageDeleteForm
     * @param jwt                     jwt
     * @return delete count
     */
    @PostMapping("/deleteThumbnail")
    public int deleteThumbnail(
            @RequestBody ProductManageDeleteForm productManageDeleteForm,
            @RequestHeader(value = "Authorization") String jwt) {
        jwtComponent.getUserFromJwt(jwt);
        pictureManageService.deleteThumbnail(productManageDeleteForm.getId());
        return 0;
    }

    /**
     * modifyThumbnail
     *
     * @param id   id
     * @param file file
     * @param jwt  jwt
     * @return insert count
     * @throws Exception Exception
     */
    @PostMapping("/modifyThumbnail")
    public int modifyThumbnail(
            @RequestParam("id") int id,
            @RequestParam("file") MultipartFile file,
            @RequestHeader(value = "Authorization") String jwt) throws Exception {
        jwtComponent.getUserFromJwt(jwt);
        int deleteCount = pictureManageService.deleteThumbnail(id);
        if (deleteCount == 1) {
            pictureManageService.insertThumbnail(id, file);
        }
        return deleteCount;
    }
}
