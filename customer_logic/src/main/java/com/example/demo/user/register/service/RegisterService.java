package com.example.demo.user.register.service;

import com.demo.common.dto.userLogin.UserKubunDto;
import com.demo.common.dto.userLogin.UserRegisterDto;

import java.util.List;

/**
 * ユーザ登録 Service
 */
public interface RegisterService {

    /**
     * ユーザを登録する
     *
     * @param userRegisterDto ユーザ情報
     * @return ユーザ登録件数
     */
    int registerByUser(UserRegisterDto userRegisterDto);

    /**
     * ユーザが登録されているか登録する
     *
     * @param email email
     * @return ユーザ検索結果件数
     */
    int checkRegisterByEmail(String email);

    /**
     * get UserKubun
     *
     * @return List<UserKubun> List<UserKubun>
     */
    List<UserKubunDto> getUserKubun();
}
