package com.example.demo.user.login.service;

import com.demo.common.dto.userLogin.UserDto;
import com.demo.common.dto.userLogin.UserLoginDto;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * ログインService
 */
public interface LoginService extends UserDetailsService {

    /**
     * ログインする
     *
     * @param userLoginDto userLoginDto
     * @return ログイン成功、ログイン失敗
     */
    UserLoginDto loginByUser(UserLoginDto userLoginDto);


    /**
     * get UserInfo
     *
     * @param email email
     * @return userInfo
     */
    UserDto getUserByEmail(String email);
}
