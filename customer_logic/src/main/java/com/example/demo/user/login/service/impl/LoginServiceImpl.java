package com.example.demo.user.login.service.impl;

import com.demo.common.dto.userLogin.UserDto;
import com.demo.common.dto.userLogin.UserLoginDto;
import com.example.demo.mapper.UserMapper;
import com.example.demo.model.User;
import com.example.demo.model.UserExample;
import com.example.demo.user.login.service.LoginService;
import com.example.demo.user.temp.TempUserDto;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * LoginServiceImpl
 */
@Service
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {

    /**
     * UserMapper
     */
    private final UserMapper userMapper;

    /**
     * PasswordEncoder
     */
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserLoginDto loginByUser(UserLoginDto userLoginDto) {

        UserExample userExample = new UserExample();
        userExample.createCriteria().andEmailEqualTo(userLoginDto.getEmail());
        List<User> selectedUsers = userMapper.selectByExample(userExample);

        if (selectedUsers == null || selectedUsers.size() == 0) {
            return null;
        }
        User selectedUser = selectedUsers.get(0);
        String encodedPassword = selectedUser.getPassword();
        String rawPassword = userLoginDto.getPassword();
        boolean password = passwordEncoder.matches(rawPassword, encodedPassword);
        if (!password) {
            return null;
        }
        userLoginDto.setUserId(selectedUser.getId());
        return userLoginDto;
    }

    @Override
    public UserDto getUserByEmail(String email) {
        UserExample userExample = new UserExample();
        userExample.createCriteria().andEmailEqualTo(email);
        List<User> selectedUsers = userMapper.selectByExample(userExample);
        User selectedUser = selectedUsers.get(0);
        ModelMapper modelMapper = new ModelMapper();
        UserDto userResult = new UserDto();
        modelMapper.map(selectedUser, userResult);
        return userResult;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserExample userExample = new UserExample();
        userExample.createCriteria().andEmailEqualTo(username);
        List<User> selectedUser = userMapper.selectByExample(userExample);

        if (selectedUser == null || selectedUser.size() == 0) {
            return null;
        }
        ModelMapper modelMapper = new ModelMapper();
        TempUserDto userTempDto = new TempUserDto();
        modelMapper.map(selectedUser.get(0), userTempDto);

        return userTempDto;
    }
}
