package com.example.demo.user.register.service.impl;

import com.demo.common.dto.userLogin.UserKubunDto;
import com.demo.common.dto.userLogin.UserRegisterDto;
import com.example.demo.mapper.UserKubunMapper;
import com.example.demo.mapper.UserMapper;
import com.example.demo.model.User;
import com.example.demo.model.UserExample;
import com.example.demo.model.UserKubun;
import com.example.demo.model.UserKubunExample;
import com.example.demo.user.register.service.RegisterService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * RegisterServiceImpl
 */
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class RegisterServiceImpl implements RegisterService {

    /**
     * userMapper
     */
    private final UserMapper userMapper;

    /**
     * userKubunMapper
     */
    private final UserKubunMapper userKubunMapper;

    /**
     * passwordEncoder
     */
    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional(readOnly = false)
    public int registerByUser(UserRegisterDto userRegisterDto) {

        if (1 <= checkRegisterByEmail(userRegisterDto.getEmail())) {
            return 0;
        }

        User user = new User();
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.map(userRegisterDto, user);
        user.setId(0);
        user.setPassword(passwordEncoder.encode(userRegisterDto.getPassword()));
        return userMapper.insert(user);
    }

    @Override
    public int checkRegisterByEmail(String email) {
        UserExample userExample = new UserExample();
        userExample.createCriteria().andEmailEqualTo(email);
        List<User> getUserList = userMapper.selectByExample(userExample);
        return getUserList.size();
    }

    @Override
    public List<UserKubunDto> getUserKubun() {
        UserKubunExample userKubunExample = new UserKubunExample();
        List<UserKubun> userKubuns = userKubunMapper.selectByExample(userKubunExample);
        List<UserKubunDto> userKubunResult = new ArrayList<>();

        ModelMapper modelMapper = new ModelMapper();

        userKubuns.forEach(obj -> {
            UserKubunDto userKubunDto = new UserKubunDto();
            modelMapper.map(obj, userKubunDto);
            userKubunResult.add(userKubunDto);
        });
        return userKubunResult;
    }
}
