package com.example.demo.userInfo.Service;

import com.demo.common.dto.userLogin.UserDto;

/**
 * UserChangeInfoService
 */
public interface UserChangeInfoService {

    /**
     * userUpdate
     *
     * @param userDto userDto
     * @param userId userId
     * @return updateCount
     */
    int userUpdate(UserDto userDto, int userId);
}
