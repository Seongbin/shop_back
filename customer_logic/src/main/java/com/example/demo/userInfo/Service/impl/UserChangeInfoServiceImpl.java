package com.example.demo.userInfo.Service.impl;

import com.demo.common.dto.userLogin.UserDto;
import com.example.demo.mapper.UserMapper;
import com.example.demo.model.User;
import com.example.demo.userInfo.Service.UserChangeInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

/**
 * UserChangeInfoServiceImpl
 */
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class UserChangeInfoServiceImpl implements UserChangeInfoService {

    /**
     * userMapper
     */
    private final UserMapper userMapper;

    /**
     * passwordEncoder
     */
    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional(readOnly = false)
    public int userUpdate(UserDto userDto, int userId) {
        User user = userMapper.selectByPrimaryKey(userId);


        if (!StringUtils.isEmpty(userDto.getPassword())) {
            user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        }

        if (!StringUtils.isEmpty(userDto.getPhoneNumber())) {
            user.setPhoneNumber(userDto.getPhoneNumber());
        }

        if (!StringUtils.isEmpty(userDto.getAddress())) {
            user.setAddress(userDto.getAddress());
        }

        user.setId(userId);
        int updateCount = userMapper.updateByPrimaryKey(user);
        return updateCount;
    }
}
