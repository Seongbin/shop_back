package com.example.demo.qnaReply.qnaReplyService;

import com.demo.common.dto.qnaReply.QnaReplyDto;
import java.util.List;

/**
 * QnaReplyService
 */
public interface QnaReplyService {

    /**
     * getQnaReplyByProductId
     *
     * @param qnaBoardId qnaBoardId
     * @return List<Qnareply>
     */
    List<QnaReplyDto> getQnaReplyByProductId(int qnaBoardId);

    /**
     * getQnaReplyByQnaId
     *
     * @param qnaReplyDto qnaReplyDto
     * @return QnaReplyDto
     */
    QnaReplyDto insertQnaReply(QnaReplyDto qnaReplyDto);
}
