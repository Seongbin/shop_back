package com.example.demo.qnaReply.qnaReplyService.impl;

import com.demo.common.dto.qnaReply.QnaReplyDto;
import com.example.demo.mapper.QnareplyMapper;
import com.example.demo.model.Qnareply;
import com.example.demo.model.QnareplyExample;
import com.example.demo.qnaReply.qnaReplyService.QnaReplyService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * QnaReplyServiceImpl
 */
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class QnaReplyServiceImpl implements QnaReplyService {

    /**
     * qnareplyMapper
     */
    private final QnareplyMapper qnareplyMapper;

    @Override
    public List<QnaReplyDto> getQnaReplyByProductId(int qnaBoardId) {
        QnareplyExample qnareplyExample = new QnareplyExample();
        qnareplyExample.createCriteria().andQnaboardIdEqualTo(qnaBoardId);
        List<Qnareply> qnareplies = qnareplyMapper.selectByExample(qnareplyExample);

        List<QnaReplyDto> qnaReplyDtos = new ArrayList<>();

        ModelMapper modelMapper = new ModelMapper();
        qnareplies.forEach(obj -> {
            QnaReplyDto qnaReplyDto = new QnaReplyDto();
            modelMapper.map(obj, qnaReplyDto);
            qnaReplyDtos.add(qnaReplyDto);
        });
        return qnaReplyDtos;
    }

    @Override
    @Transactional(readOnly = false)
    public QnaReplyDto insertQnaReply(QnaReplyDto qnaReplyDto) {
        Qnareply qnaReply = new Qnareply();
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.map(qnaReplyDto, qnaReply);
        qnareplyMapper.insert(qnaReply);
        return qnaReplyDto;
    }
}
