package com.example.demo.order.service.impl;

import com.demo.common.dto.order.OrderDto;
import com.demo.common.dto.order.OrdersDto;
import com.demo.common.enumPackage.OrderEnum;
import com.example.demo.mapper.OrderCustomMapper;
import com.example.demo.mapper.OrderMapper;
import com.example.demo.model.Order;
import com.example.demo.model.OrderExample;
import com.example.demo.model.Orders;
import com.example.demo.order.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * OrderServiceImpl
 */
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class OrderServiceImpl implements OrderService {

    /**
     * orderMapper
     */
    private final OrderMapper orderMapper;

    /**
     * orderCustomMapper
     */
    private final OrderCustomMapper orderCustomMapper;

    @Override
    public List<OrdersDto> getOrdersByUserId(int userId) {
        List<Orders> orders = orderCustomMapper.getOrdersByUserId(userId);

        List<OrdersDto> result = new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();
        orders.forEach(obj -> {
            OrdersDto ordersDto = new OrdersDto();
            modelMapper.map(obj, ordersDto);
            result.add(ordersDto);
        });
        return result;
    }

    @Override
    @Transactional(readOnly = false)
    public int order(List<OrderDto> ordersDto, String address, int userId) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        AtomicInteger insertedCount = new AtomicInteger();

        ordersDto.forEach(orderDto -> {
            Order order = new Order();
            modelMapper.map(orderDto, order);
            order.setAddress(address);
            order.setOrderStateId(OrderEnum.OrderStart.getOrderEnum());
            order.setUserId(userId);
            insertedCount.addAndGet(orderMapper.insert(order));
        });

        return insertedCount.get();
    }

    @Override
    @Transactional(readOnly = false)
    public int cancelOrdersByOrderId(ArrayList<Integer> orderIds) {
        OrderExample orderExample = new OrderExample();
        orderExample.createCriteria().andOrderStateIdEqualTo(OrderEnum.OrderCancel.getOrderEnum());
        AtomicInteger updatedCount = new AtomicInteger();

        orderIds.forEach(orderId -> {
            Order order = new Order();
            order.setId(orderId);
            int count = orderMapper.updateByExampleSelective(order, orderExample);
            updatedCount.addAndGet(count);
        });
        return updatedCount.get();
    }
}
