package com.example.demo.order.service;

import com.demo.common.dto.order.OrderDto;
import com.demo.common.dto.order.OrdersDto;

import java.util.ArrayList;
import java.util.List;

/**
 * 注文Service
 */
public interface OrderService {

    /**
     * 注文リストを取得する
     *
     * @param userId userId
     * @return List<Orders>
     */
    List<OrdersDto> getOrdersByUserId(int userId);

    /**
     * 注文する
     *
     * @param ordersDto ordersDto
     * @param address address
     * @param userId userId
     * @return 注文件数
     */
    int order(List<OrderDto> ordersDto, String address, int userId);

    /**
     * 注文取消する
     *
     * @param orderIds orderIds
     * @return 取消件数件数
     */
    int cancelOrdersByOrderId(ArrayList<Integer> orderIds);
}
