package com.example.demo.cart.service;

import com.demo.common.dto.cart.CartDto;
import com.demo.common.dto.cart.CartsDto;

import java.util.ArrayList;
import java.util.List;

/**
 * カートService
 */
public interface CartService {

    /**
     * add Cart
     *
     * @param cartDto cartDto
     * @return inserted count
     */
    int addCart(CartDto cartDto);

    /**
     * get cart
     *
     * @param userId userId
     * @return List<Carts>
     */
    List<CartsDto> getCart(int userId);

    /**
     * delete carts
     *
     * @param cartIds cart ids
     * @return 削除件数
     */
    int deleteCarts(ArrayList<Integer> cartIds);
}
