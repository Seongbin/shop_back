package com.example.demo.cart.service.impl;

import com.demo.common.dto.cart.CartDto;
import com.demo.common.dto.cart.CartsDto;
import com.example.demo.cart.service.CartService;
import com.example.demo.mapper.CartCustomMapper;
import com.example.demo.mapper.CartMapper;
import com.example.demo.model.Cart;
import com.example.demo.model.CartExample;
import com.example.demo.model.Carts;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * CartServiceImpl
 */
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class CartServiceImpl implements CartService {

    /**
     * CartMapper
     */
    private final CartMapper cartMapper;

    /**
     * CartCustomMapper
     */
    private final CartCustomMapper cartCustomMapper;

    @Override
    @Transactional(readOnly = false)
    public int addCart(CartDto cartDto) {
        ModelMapper modelMapper = new ModelMapper();
        Cart cart = new Cart();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        modelMapper.map(cartDto, cart);
        int insertedCount = cartMapper.insert(cart);
        return insertedCount;
    }

    @Override
    public List<CartsDto> getCart(int userId) {
        List<Carts> cartsList = cartCustomMapper.getCartsByUserId(userId);
        List<CartsDto> result = new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();
        cartsList.forEach(obj -> {
            CartsDto cartsDto = modelMapper.map(obj, CartsDto.class);
            result.add(cartsDto);
        });
        return result;
    }

    @Override
    @Transactional(readOnly = false)
    public int deleteCarts(ArrayList<Integer> cartIds) {
        CartExample cartExample = new CartExample();
        cartExample.createCriteria().andIdIn(cartIds);
        int deleteCount = cartMapper.deleteByExample(cartExample);
        return deleteCount;
    }
}
