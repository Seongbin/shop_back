package com.example.demo.product.service;

import com.demo.common.dto.product.ProductDetailDto;
import com.demo.common.dto.product.ProductDto;


import java.util.List;

/**
 * 商品Service
 */
public interface ProductService {

    //TODO TOP - N query
    /**
     * 商品リストを取得する
     * @return List<ProductDto>
     */
    List<ProductDto> getProducts();

    /**
     * 商品を取得する
     *
     * @param productId productId
     * @return 商品詳細
     */
    ProductDetailDto getProduct(int productId);

}
