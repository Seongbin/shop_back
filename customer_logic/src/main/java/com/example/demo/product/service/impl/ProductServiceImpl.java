package com.example.demo.product.service.impl;

import com.demo.common.dto.product.*;
import com.demo.common.dto.qna.QnaboardDto;
import com.example.demo.mapper.*;
import com.example.demo.model.*;
import com.example.demo.product.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * ProductServiceImpl
 */
@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    /**
     * ProductMapper
     */
    private final ProductMapper productMapper;

    /**
     * ProductPictureMapper
     */
    private final ProductPictureMapper productPictureMapper;

    /**
     * ProductDescriptionMapper
     */
    private final ProductDescriptionMapper productDescriptionMapper;

    /**
     * ProductOptionMapper
     */
    private final ProductOptionMapper productOptionMapper;

    /**
     * QnaboardMapper
     */
    private final QnaboardMapper qnaboardMapper;

    //TODO TOP - N query
    @Override
    public List<ProductDto> getProducts() {
        List<Product> products = productMapper.selectByExample(new ProductExample());
        List<ProductDto> resultProducts = new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();
        products.forEach(obj -> {
            ProductDto productDto = new ProductDto();
            modelMapper.map(obj, productDto);
            resultProducts.add(productDto);
        });
        return resultProducts;
    }

    @Override
    public ProductDetailDto getProduct(int productId) {
        ModelMapper modelMapper = new ModelMapper();
        ProductDetailDto productDetailDto = new ProductDetailDto();

        ProductOptionExample productOptionExample = new ProductOptionExample();
        productOptionExample.createCriteria().andProductIdEqualTo(productId);
        List<ProductOption> productOptions = productOptionMapper.selectByExample(productOptionExample);
        List<ProductOptionDto> productOptionDtos = new ArrayList<>();
        productOptions.forEach(obj -> {
            ProductOptionDto productOptionDto = new ProductOptionDto();
            modelMapper.map(obj, productOptionDto);
            productOptionDtos.add(productOptionDto);
        });

        ProductPictureExample productPictureExample = new ProductPictureExample();
        productPictureExample.createCriteria().andProductIdEqualTo(productId);
        List<ProductPicture> productPictures = productPictureMapper.selectByExample(productPictureExample);
        List<ProductPictureDto> productPicturesDtos = new ArrayList<>();
        productPictures.forEach(obj -> {
            ProductPictureDto productPictureDto = new ProductPictureDto();
            modelMapper.map(obj, productPictureDto);
            productPicturesDtos.add(productPictureDto);
        });

        ProductDescriptionExample productDescriptionExample = new ProductDescriptionExample();
        productDescriptionExample.createCriteria().andProductIdEqualTo(productId);
        List<ProductDescription> productDescriptions = productDescriptionMapper.selectByExample(productDescriptionExample);
        List<ProductDescriptionDto> productDescriptionsDtos = new ArrayList<>();
        productDescriptions.forEach(obj -> {
            ProductDescriptionDto productDescriptionDto = new ProductDescriptionDto();
            modelMapper.map(obj, productDescriptionDto);
            productDescriptionsDtos.add(productDescriptionDto);
        });


        QnaboardExample qnaboardExample = new QnaboardExample();
        qnaboardExample.createCriteria().andProductIdEqualTo(productId);
        List<Qnaboard> qnaboards = qnaboardMapper.selectByExample(qnaboardExample);
        List<QnaboardDto> qnaboardDtos = new ArrayList<>();
        qnaboards.forEach(obj -> {
            QnaboardDto qnaboardDto = new QnaboardDto();
            modelMapper.map(obj, qnaboardDto);
            qnaboardDtos.add(qnaboardDto);
        });

        Product product = productMapper.selectByPrimaryKey(productId);
        ProductDto productDto = new ProductDto();
        modelMapper.map(product, productDto);

        productDetailDto.setProductPictures(productPicturesDtos);
        productDetailDto.setProductDescriptions(productDescriptionsDtos);
        productDetailDto.setProductOptions(productOptionDtos);
        productDetailDto.setQnaBoards(qnaboardDtos);
        productDetailDto.setProductDto(productDto);
        return productDetailDto;
    }
}
