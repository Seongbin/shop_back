package com.example.demo.qna.service;

import com.demo.common.dto.qna.QnaboardDto;

import java.util.List;

/**
 * QnaService
 */
public interface QnaService {

    /**
     * getQnaBoardByProductId
     *
     * @param productId productId
     * @return List<Qnaboard>
     */
    List<QnaboardDto> getQnaBoardByProductId(int productId);

    /**
     * getQnaById
     *
     * @param id id
     * @return Qnaboard
     */
    QnaboardDto getQnaById(int id);

    /**
     * insertQna
     *
     * @param qnaboardDto qnaboardDto
     * @return 入力件数
     */
    int insertQna(QnaboardDto qnaboardDto);
}
