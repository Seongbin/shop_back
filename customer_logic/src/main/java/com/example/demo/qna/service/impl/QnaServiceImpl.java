package com.example.demo.qna.service.impl;

import com.demo.common.dto.qna.QnaboardDto;
import com.example.demo.mapper.QnaboardMapper;
import com.example.demo.model.Qnaboard;
import com.example.demo.model.QnaboardExample;
import com.example.demo.qna.service.QnaService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * QnaServiceImpl
 */
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class QnaServiceImpl implements QnaService {

    /**
     * QnaboardMapper
     */
    private final QnaboardMapper qnaboardMapper;

    @Override
    public List<QnaboardDto> getQnaBoardByProductId(int productId) {
        QnaboardExample qnaboardExample = new QnaboardExample();
        qnaboardExample.createCriteria().andProductIdEqualTo(productId);
        List<Qnaboard> qnaboards = qnaboardMapper.selectByExample(qnaboardExample);
        List<QnaboardDto> qnaboardList = new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();

        qnaboardList.forEach(obj -> {
            QnaboardDto qnaBoardDto = new QnaboardDto();
            modelMapper.map(obj, qnaBoardDto);
            qnaboardList.add(qnaBoardDto);
        });
        return qnaboardList;
    }

    @Override
    public QnaboardDto getQnaById(int id) {
        Qnaboard qnaboard = qnaboardMapper.selectByPrimaryKey(id);
        QnaboardDto qnaBoardDto = new QnaboardDto();
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.map(qnaboard, qnaBoardDto);
        return qnaBoardDto;
    }

    @Override
    @Transactional(readOnly = false)
    public int insertQna(QnaboardDto qnaboardDto) {
        ModelMapper modelMapper = new ModelMapper();
        Qnaboard qnaBoard = new Qnaboard();
        modelMapper.map(qnaboardDto, qnaBoard);
        int insertCount = qnaboardMapper.insert(qnaBoard);
        return insertCount;
    }
}
