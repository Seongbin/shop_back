package com.example.demo.order.controller;

import com.demo.common.dto.order.OrderDto;
import com.demo.common.dto.order.OrdersDto;
import com.demo.common.dto.userLogin.UserLoginDto;
import com.demo.common.form.order.DeleteOrderForm;
import com.demo.common.form.order.OrderForm;
import com.demo.common.jwt.JwtComponent;
import com.example.demo.order.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 注文Controller
 */
@RestController
@RequiredArgsConstructor
public class OrderController {

    /**
     * OrderService
     */
    private final OrderService orderService;

    /**
     * jwtComponent
     */
    private final JwtComponent jwtComponent;

    /**
     * 注文履歴取得
     *
     * @param jwt jwt
     * @return orders
     */
    @GetMapping("/orders")
    public List<OrdersDto> orders(
            @RequestHeader(value = "Authorization") String jwt) {
        UserLoginDto userLoginDto = jwtComponent.getUserFromJwt(jwt);
        List<OrdersDto> orders = orderService.getOrdersByUserId(userLoginDto.getUserId());
        return orders;
    }

    /**
     * 注文する
     *
     * @param jwt       jwt
     * @param orderForm orderForm
     * @return 注文件数
     */
    @PostMapping("/order")
    public int order(
            @RequestHeader(value = "Authorization") String jwt,
            @Valid @RequestBody OrderForm orderForm) {
        UserLoginDto userLoginDto = jwtComponent.getUserFromJwt(jwt);
        List<OrderDto> ordersDto = orderForm.getOrdersDto();
        String address = orderForm.getAddress();
        int insertCount = orderService.order(ordersDto, address, userLoginDto.getUserId());
        return insertCount;
    }

    /**
     * @param deleteOrderForm deleteOrderForm
     * @return 取消件数
     */
    @PostMapping("/cancelOrder")
    public int cancelOrder(
            @Valid @RequestBody DeleteOrderForm deleteOrderForm) {
        int cancelCount = orderService.cancelOrdersByOrderId(deleteOrderForm.getOrderIds());
        return cancelCount;
    }
}
