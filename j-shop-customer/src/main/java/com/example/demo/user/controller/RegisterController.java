package com.example.demo.user.controller;

import com.demo.common.dto.userLogin.UserKubunDto;
import com.demo.common.dto.userLogin.UserRegisterDto;
import com.demo.common.form.user.UserRegisterForm;
import com.example.demo.user.register.service.RegisterService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

/**
 * ユーザ登録 Controller
 */
@RestController
@RequiredArgsConstructor
public class RegisterController {

    /**
     * registerService
     */
    private final RegisterService registerService;

    /**
     * ユーザ登録
     *
     * @param userRegisterForm userRegisterForm
     * @return success or Error
     */
    @PostMapping("/register")
    public String register(@Valid @RequestBody UserRegisterForm userRegisterForm) {
        ModelMapper modelMapper = new ModelMapper();
        UserRegisterDto userRegisterDto = new UserRegisterDto();
        modelMapper.map(userRegisterForm, userRegisterDto);
        int insertedUserCount = registerService.registerByUser(userRegisterDto);

        if (insertedUserCount == 0) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "User Duplicated");
        }

        return "success";
    }

    /**
     * get UserKubun
     *
     * @return List<UserKubun>
     */
    @GetMapping("getUserKubun")
    public List<UserKubunDto> getUserKubun() {
        List<UserKubunDto> userKubunResult = registerService.getUserKubun();
        return userKubunResult;
    }
}
