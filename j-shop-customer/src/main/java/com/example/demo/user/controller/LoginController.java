package com.example.demo.user.controller;

import com.demo.common.dto.userLogin.UserDto;
import com.demo.common.dto.userLogin.UserLoginDto;
import com.demo.common.form.user.UserLoginForm;
import com.demo.common.jwt.JwtComponent;
import com.example.demo.user.login.service.LoginService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

/**
 * ログインController
 */
@RestController
@RequiredArgsConstructor
public class LoginController {

    /**
     * LoginService
     */
    private final LoginService loginService;

    /**
     * JwtComponent
     */
    private final JwtComponent jwtComponent;

    /**
     * ログインする
     *
     * @param userLoginForm userLoginForm
     * @return jwt
     */
    @PostMapping("/login")
    public String login(@Valid @RequestBody UserLoginForm userLoginForm) {
        ModelMapper modelMapper = new ModelMapper();
        UserLoginDto userLoginDto = new UserLoginDto();
        modelMapper.map(userLoginForm, userLoginDto);
        UserLoginDto loginResult = loginService.loginByUser(userLoginDto);

        if (loginResult == null) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Login failed");
        }
        String result = jwtComponent.jwtGenerate(loginResult.getUserId(), userLoginDto.getEmail());
        return result;
    }

    /**
     * ユーザをロード
     *
     * @param jwt jwt
     * @return UserDto ユーザ情報
     */
    @PostMapping("/loadUser")
    public UserDto loadUser(@RequestHeader(value = "Authorization") String jwt) {
        UserLoginDto userLoginDto = jwtComponent.getUserFromJwt(jwt);
        UserDto userDto = loginService.getUserByEmail(userLoginDto.getEmail());
        userDto.setPassword("");
        return userDto;
    }
}
