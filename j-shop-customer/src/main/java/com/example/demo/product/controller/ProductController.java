package com.example.demo.product.controller;

import com.demo.common.dto.product.ProductDetailDto;
import com.demo.common.dto.product.ProductDto;
import com.example.demo.product.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 商品Controller
 */
@RestController
@RequiredArgsConstructor
public class ProductController {

    /**
     * ProductService
     */
    private final ProductService productService;

    /**
     * 商品リストを取得する
     * @return List<ProductDto> 商品リスト
     */
    @GetMapping("/products")
    public List<ProductDto> getProductFirst() {
        List<ProductDto> products = productService.getProducts();
        return products;
    }

    /**
     * Product idで商品を取得する
     *
     * @param productId Product id
     * @return 商品
     */
    @GetMapping("/product/{productId}")
    public ProductDetailDto getProductFirst(@PathVariable int productId) {
        ProductDetailDto product = productService.getProduct(productId);
        return product;
    }
}
