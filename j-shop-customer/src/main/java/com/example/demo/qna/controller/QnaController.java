package com.example.demo.qna.controller;

import com.demo.common.dto.qna.QnaboardDto;
import com.demo.common.dto.userLogin.UserLoginDto;
import com.demo.common.form.qna.QnaboardForm;
import com.demo.common.jwt.JwtComponent;
import com.example.demo.qna.service.QnaService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * QnaController
 */
@RestController
@RequiredArgsConstructor
public class QnaController {

    /**
     * qnaService
     */
    private final QnaService qnaService;

    /**
     * jwtComponent
     */
    private final JwtComponent jwtComponent;

    /**
     * getQnaList by Product id
     *
     * @param productId productId
     * @return List<QnaboardDto>
     */
    @GetMapping("/qnas/{productId}")
    public List<QnaboardDto> getQnasByProductId(@PathVariable(name = "productId") int productId) {
        List<QnaboardDto> qnaboardList = qnaService.getQnaBoardByProductId(productId);
        return qnaboardList;
    }

    /**
     * getQnaByid
     *
     * @param id id
     * @return QnaboardDto
     */
    @GetMapping("/qna/{id}")
    public QnaboardDto getQnaByid(@PathVariable(name = "id") int id) {
        QnaboardDto qnaboard = qnaService.getQnaById(id);
        return qnaboard;
    }

    @PostMapping("/qna")
    public QnaboardDto qnaboard(
            @RequestHeader(value = "Authorization") String jwt,
            @RequestBody QnaboardForm qnaboardForm) {
        UserLoginDto userLoginDto = jwtComponent.getUserFromJwt(jwt);
        QnaboardDto qnaBoardDto = new QnaboardDto();
        ModelMapper modelmapper = new ModelMapper();
        modelmapper.map(qnaboardForm, qnaBoardDto);
        qnaBoardDto.setId(null);
        qnaBoardDto.setUserId(userLoginDto.getUserId());
        qnaService.insertQna(qnaBoardDto);
        return qnaBoardDto;
    }
}
