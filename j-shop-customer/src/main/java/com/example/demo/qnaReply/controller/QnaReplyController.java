package com.example.demo.qnaReply.controller;

import com.demo.common.dto.qnaReply.QnaReplyDto;
import com.demo.common.dto.userLogin.UserLoginDto;
import com.demo.common.form.qnaReply.QnaReplyForm;
import com.demo.common.jwt.JwtComponent;
import com.example.demo.qnaReply.qnaReplyService.QnaReplyService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * QnaReplyController
 */
@RestController
@RequiredArgsConstructor
public class QnaReplyController {

    /**
     * qnaReplyService
     */
    private final QnaReplyService qnaReplyService;

    /**
     * jwtComponent
     */
    private final JwtComponent jwtComponent;

    /**
     * getQnaReplyByProductId
     *
     * @param qnaBoardId qnaBoardId
     * @return List<Qnareply>
     */
    @GetMapping("/qnaReply/{qnaBoardId}")
    public List<QnaReplyDto> getQnaReplyByProductId(@PathVariable(name = "qnaBoardId") int qnaBoardId) {
        List<QnaReplyDto> qnaReplys = qnaReplyService.getQnaReplyByProductId(qnaBoardId);
        return qnaReplys;
    }

    /**
     * qnaReply
     *
     * @param qnaReplyForm qnaReplyForm
     * @param jwt          jwt
     */
    @PostMapping("/qnaReply")
    public void qnaReply(
            @RequestBody QnaReplyForm qnaReplyForm,
            @RequestHeader(name = "Authorization") String jwt) {
        UserLoginDto userLoginDto = jwtComponent.getUserFromJwt(jwt);
        ModelMapper modelMapper = new ModelMapper();
        QnaReplyDto qnareplyDto = new QnaReplyDto();
        modelMapper.map(qnaReplyForm, qnareplyDto);
        qnareplyDto.setUserId(userLoginDto.getUserId());
        qnareplyDto.setId(null);
        qnaReplyService.insertQnaReply(qnareplyDto);
    }
}
