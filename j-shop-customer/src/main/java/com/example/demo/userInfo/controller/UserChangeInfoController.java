package com.example.demo.userInfo.controller;

import com.demo.common.dto.userLogin.UserDto;
import com.demo.common.dto.userLogin.UserLoginDto;
import com.demo.common.form.user.UserChangeInfoForm;
import com.demo.common.jwt.JwtComponent;
import com.example.demo.userInfo.Service.UserChangeInfoService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 * UserChangeInfoController
 */
@RestController
@RequiredArgsConstructor
public class UserChangeInfoController {

    /**
     * userChangeInfoService
     */
    private final UserChangeInfoService userChangeInfoService;

    /**
     * jwtComponent
     */
    private final JwtComponent jwtComponent;

    /**
     * changeUserInfo
     *
     * @param userChangeInfoForm userChangeInfoForm
     * @param jwt                jwt
     * @return UserDto
     */
    @PostMapping("/changeUserInfo")
    public UserDto changeUserInfo(
            @RequestBody UserChangeInfoForm userChangeInfoForm,
            @RequestHeader(value = "Authorization") String jwt) {
        UserLoginDto userLoginDto = jwtComponent.getUserFromJwt(jwt);
        int userId = userLoginDto.getUserId();
        ModelMapper modelMapper = new ModelMapper();
        UserDto userDto = new UserDto();
        modelMapper.map(userChangeInfoForm, userDto);
        int updateCount = userChangeInfoService.userUpdate(userDto, userId);
        if (updateCount == 0) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "User Not ");
        }
        return userDto;
    }
}
