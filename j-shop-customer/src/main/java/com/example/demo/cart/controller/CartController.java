package com.example.demo.cart.controller;

import com.demo.common.dto.cart.CartDto;
import com.demo.common.dto.cart.CartsDto;
import com.demo.common.dto.userLogin.UserLoginDto;
import com.demo.common.form.cart.CartForm;
import com.demo.common.form.cart.DeleteCartForm;
import com.demo.common.jwt.JwtComponent;
import com.example.demo.cart.service.CartService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * カートController
 */
@RestController
@RequiredArgsConstructor
public class CartController {

    /**
     * CartService
     */
    private final CartService cartService;

    /**
     * JwtComponent
     */
    private final JwtComponent jwtComponent;

    /**
     * カートに追加する
     *
     * @param jwt      認証のためのJWT
     * @param cartForm cartForm
     * @return カート登録件数
     */
    @PostMapping("/cart")
    public int addCart(
            @RequestHeader(value = "Authorization") String jwt,
            @Valid @RequestBody CartForm cartForm) {
        ModelMapper modelMapper = new ModelMapper();
        CartDto cartDto = new CartDto();
        modelMapper.map(cartForm, cartDto);
        UserLoginDto userLoginDto = jwtComponent.getUserFromJwt(jwt);
        cartDto.setUserId(userLoginDto.getUserId());
        return cartService.addCart(cartDto);
    }

    /**
     * カートリストを取得する
     *
     * @param jwt 認証のためのJWT
     * @return List<CartsDto> カートリスト
     */
    @GetMapping("/carts")
    public List<CartsDto> getCart(@RequestHeader(value = "Authorization") String jwt) {
        UserLoginDto userLoginDto = jwtComponent.getUserFromJwt(jwt);
        List<CartsDto> result = cartService.getCart(userLoginDto.getUserId());
        return result;
    }

    /**
     * カートリストを削除します
     *
     * @param deleteCartForm deleteCartForm
     * @return 削除件数
     */
    @PostMapping("/deleteCarts")
    public int deleteCarts(@RequestBody DeleteCartForm deleteCartForm) {
        int deleteCount = cartService.deleteCarts(deleteCartForm.getCartIds());
        return deleteCount;
    }
}
