package com.company;

import java.io.*;
import java.math.BigDecimal;

public class Main {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        String userInput = br.readLine();
        BigDecimal bigDecimal = new BigDecimal(userInput);
        bw.write(bigDecimal.remainder(new BigDecimal("20000303")).toString());
        bw.flush();
        br.close();
        bw.close();
    }
}
