package com.example.gateway.controller;

import com.demo.common.dto.orderManage.OrderStateOrderDto;
import com.demo.common.dto.product.ProductDetailDto;
import com.demo.common.dto.product.ProductDto;
import com.demo.common.form.orderManage.OrderStateOrderForm;
import com.demo.common.form.productManage.AddProductForm;
import com.demo.common.form.productManage.ModifyProductForm;
import com.demo.common.form.productManage.ProductManageDeleteForm;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ManageGateway {
    @Value("${url.manage.url}")
    private String url;

    @Value("${url.manage.port}")
    private String port;

    private final RestTemplate restTemplate;

    /**
     * getOrderManages
     *
     * @param jwt jwt
     * @return List<OrderStateOrder>
     */
    @GetMapping("/orderManages")
    public List<OrderStateOrderDto> getOrderManages(
            @RequestHeader(name = "Authorization") String jwt) {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwt);
        final HttpEntity<HttpHeaders> entity = new HttpEntity<>(headers);
//        ResponseEntity<List> response = restTemplate.exchange(url + ":" + port + "/orderManages", HttpMethod.GET, entity, List.class);
        ResponseEntity<List> response = restTemplate.exchange(url + "/orderManages", HttpMethod.GET, entity, List.class);
        return response.getBody();
    }

    /**
     * orderState
     *
     * @param orderStateOrderForm orderStateOrderForm
     * @param jwt                 jwt
     */
    @PostMapping("/orderState")
    public int orderState(
            @RequestBody OrderStateOrderForm orderStateOrderForm,
            @RequestHeader(name = "Authorization") String jwt
    ) {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwt);
        final HttpEntity<OrderStateOrderForm> entity = new HttpEntity<>(orderStateOrderForm, headers);
//        ResponseEntity<Integer> response = restTemplate.exchange(url + ":" + port + "/orderState", HttpMethod.POST, entity, Integer.class);
        ResponseEntity<Integer> response = restTemplate.exchange(url + "/orderState", HttpMethod.POST, entity, Integer.class);
        return response.getBody();
    }


    /**
     * getProductManage
     *
     * @param jwt jwt
     * @return List<ProductDto>
     */
    @GetMapping("/productManages")
    public List<ProductDto> getProductManage(@RequestHeader(value = "Authorization") String jwt) {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwt);
        final HttpEntity<HttpHeaders> entity = new HttpEntity<>(headers);
//        ResponseEntity<List> response = restTemplate.exchange(url + ":" + port + "/productManages", HttpMethod.GET, entity, List.class);
        ResponseEntity<List> response = restTemplate.exchange(url + "/productManages", HttpMethod.GET, entity, List.class);
        return response.getBody();
    }

    /**
     * addProductManage
     *
     * @param addProductForm addProductForm
     * @param jwt            jwt
     * @return productId
     */
    @PostMapping("/addProduct")
    public int addProductManage(
            @RequestBody AddProductForm addProductForm,
            @RequestHeader(value = "Authorization") String jwt) {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwt);
        final HttpEntity<AddProductForm> entity = new HttpEntity<>(addProductForm, headers);
//        ResponseEntity<Integer> response = restTemplate.exchange(url + ":" + port + "/addProduct", HttpMethod.POST, entity, Integer.class);
        ResponseEntity<Integer> response = restTemplate.exchange(url + "/addProduct", HttpMethod.POST, entity, Integer.class);
        return response.getBody();
    }

    /**
     * getModifyProduct
     *
     * @param jwt       jwt
     * @param productId productId
     * @return ProductDetailDto
     */
    @GetMapping("/modifyProduct/{productId}")
    public ProductDetailDto getModifyProduct(
            @RequestHeader(value = "Authorization") String jwt,
            @PathVariable(name = "productId") int productId) {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwt);
        final HttpEntity<HttpHeaders> entity = new HttpEntity<>(headers);
//        ResponseEntity<ProductDetailDto> response = restTemplate.exchange(url + ":" + port + "/modifyProduct/" + productId, HttpMethod.GET, entity, ProductDetailDto.class);
        ResponseEntity<ProductDetailDto> response = restTemplate.exchange(url  + "/modifyProduct/" + productId, HttpMethod.GET, entity, ProductDetailDto.class);

        return response.getBody();
    }

    /**
     * modifyProductInfo
     *
     * @param jwt jwt
     * @return ProductDetailDto
     */
    @PostMapping("/modifyProduct")
    public ProductDetailDto modifyProduct(
            @RequestBody ModifyProductForm modifyProductForm,
            @RequestHeader(value = "Authorization") String jwt) {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwt);
        final HttpEntity<ModifyProductForm> entity = new HttpEntity<>(modifyProductForm, headers);
//        ResponseEntity<ProductDetailDto> response = restTemplate.exchange(url + ":" + port + "/modifyProduct", HttpMethod.POST, entity, ProductDetailDto.class);
        ResponseEntity<ProductDetailDto> response = restTemplate.exchange(url + "/modifyProduct", HttpMethod.POST, entity, ProductDetailDto.class);

        return response.getBody();
    }

    /**
     * deleteProduct
     *
     * @param productManageDeleteForm productManageDeleteForm
     * @param jwt                     jwt
     * @return delete count
     */
    @PostMapping("/deleteProduct")
    public int deleteProduct(
            @RequestBody ProductManageDeleteForm productManageDeleteForm,
            @RequestHeader(value = "Authorization") String jwt) {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwt);
        final HttpEntity<ProductManageDeleteForm> entity = new HttpEntity<>(productManageDeleteForm, headers);
//        ResponseEntity<Integer> response = restTemplate.exchange(url + ":" + port + "/deleteProduct", HttpMethod.POST, entity, Integer.class);
        ResponseEntity<Integer> response = restTemplate.exchange(url + "/deleteProduct", HttpMethod.POST, entity, Integer.class);
        return response.getBody();
    }
}
