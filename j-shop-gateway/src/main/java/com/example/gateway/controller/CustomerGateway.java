package com.example.gateway.controller;

import com.demo.common.dto.cart.CartsDto;
import com.demo.common.dto.order.OrdersDto;
import com.demo.common.dto.product.ProductDetailDto;
import com.demo.common.dto.product.ProductDto;
import com.demo.common.dto.qna.QnaboardDto;
import com.demo.common.dto.userLogin.UserDto;
import com.demo.common.dto.userLogin.UserKubunDto;
import com.demo.common.form.cart.CartForm;
import com.demo.common.form.cart.DeleteCartForm;
import com.demo.common.form.order.DeleteOrderForm;
import com.demo.common.form.order.OrderForm;
import com.demo.common.form.qna.QnaboardForm;
import com.demo.common.form.user.UserChangeInfoForm;
import com.demo.common.form.user.UserLoginForm;
import com.demo.common.form.user.UserRegisterForm;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class CustomerGateway {

    @Value("${url.customer.url}")
    private String url;

    @Value("${url.customer.port}")
    private String port;

    private final RestTemplate restTemplate;

    /**
     * カートに追加する
     *
     * @param jwt      認証のためのJWT
     * @param cartForm cartForm
     * @return カート登録件数
     */
    @PostMapping("/cart")
    public int addCart(
            @RequestHeader(value = "Authorization") String jwt,
            @Valid @RequestBody CartForm cartForm) {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwt);
        final HttpEntity<CartForm> entity = new HttpEntity<>(cartForm, headers);
//        ResponseEntity<Integer> response = restTemplate.exchange(url + ":" + port + "/cart", HttpMethod.POST, entity, Integer.class);
        ResponseEntity<Integer> response = restTemplate.exchange(url + "/cart", HttpMethod.POST, entity, Integer.class);
        return response.getBody();
    }

    /**
     * カートリストを取得する
     *
     * @param jwt 認証のためのJWT
     * @return List<CartsDto> カートリスト
     */
    @GetMapping("/carts")
    public List<CartsDto> getCart(@RequestHeader(value = "Authorization") String jwt) {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwt);
        final HttpEntity<HttpHeaders> entity = new HttpEntity<>(headers);
//        ResponseEntity<List> response = restTemplate.exchange(url + ":" + port + "/carts", HttpMethod.GET, entity, List.class);
        ResponseEntity<List> response = restTemplate.exchange(url + "/carts", HttpMethod.GET, entity, List.class);
        return response.getBody();
    }

    /**
     * カートリストを削除します
     *
     * @param deleteCartForm deleteCartForm
     * @return 削除件数
     */
    @PostMapping("/deleteCarts")
    public int deleteCarts(@RequestBody DeleteCartForm deleteCartForm) {
        final HttpEntity<DeleteCartForm> entity = new HttpEntity<>(deleteCartForm);
//        ResponseEntity<Integer> response = restTemplate.exchange(url + ":" + port + "/deleteCarts", HttpMethod.POST, entity, Integer.class);
        ResponseEntity<Integer> response = restTemplate.exchange(url + "/deleteCarts", HttpMethod.POST, entity, Integer.class);
        return response.getBody();
    }

    /**
     * 注文履歴取得
     *
     * @param jwt jwt
     * @return orders
     */
    @GetMapping("/orders")
    public List<OrdersDto> orders(
            @RequestHeader(value = "Authorization") String jwt) {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwt);
        final HttpEntity<HttpHeaders> entity = new HttpEntity<>(headers);
//        ResponseEntity<List> response = restTemplate.exchange(url + ":" + port + "/orders", HttpMethod.GET, entity, List.class);
        ResponseEntity<List> response = restTemplate.exchange(url + "/orders", HttpMethod.GET, entity, List.class);
        return response.getBody();
    }

    /**
     * 注文する
     *
     * @param jwt       jwt
     * @param orderForm orderForm
     * @return 注文件数
     */
    @PostMapping("/order")
    public int order(
            @RequestHeader(value = "Authorization") String jwt,
            @Valid @RequestBody OrderForm orderForm) {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwt);
        final HttpEntity<OrderForm> entity = new HttpEntity<>(orderForm, headers);
//        ResponseEntity<Integer> response = restTemplate.exchange(url + ":" + port + "/order", HttpMethod.POST, entity, Integer.class);
        ResponseEntity<Integer> response = restTemplate.exchange(url + "/order", HttpMethod.POST, entity, Integer.class);
        return response.getBody();
    }

    /**
     * @param deleteOrderForm deleteOrderForm
     * @return 取消件数
     */
    @PostMapping("/cancelOrder")
    public int cancelOrder(
            @Valid @RequestBody DeleteOrderForm deleteOrderForm) {
        final HttpHeaders headers = new HttpHeaders();
        final HttpEntity<DeleteOrderForm> entity = new HttpEntity<>(deleteOrderForm);
//        ResponseEntity<Integer> response = restTemplate.exchange(url + ":" + port + "/cancelOrder", HttpMethod.POST, entity, Integer.class);
        ResponseEntity<Integer> response = restTemplate.exchange(url + "/cancelOrder", HttpMethod.POST, entity, Integer.class);
        return response.getBody();
    }


    /**
     * 商品リストを取得する
     *
     * @return List<ProductDto> 商品リスト
     */
    @GetMapping("/products")
    public List<ProductDto> getProductFirst() {
//        ResponseEntity<List> response = restTemplate.exchange(url + ":" + port + "/products", HttpMethod.GET, null, List.class);
        ResponseEntity<List> response = restTemplate.exchange(url + "/products", HttpMethod.GET, null, List.class);
        return response.getBody();
    }

    /**
     * Product idで商品を取得する
     *
     * @param productId Product id
     * @return 商品
     */
    @GetMapping("/product/{productId}")
    public ProductDetailDto getProductFirst(@PathVariable int productId) {
//        ResponseEntity<ProductDetailDto> response = restTemplate.exchange(url + ":" + port + "/product/" + productId, HttpMethod.GET, null, ProductDetailDto.class);
        ResponseEntity<ProductDetailDto> response = restTemplate.exchange(url + "/product/" + productId, HttpMethod.GET, null, ProductDetailDto.class);

        return response.getBody();
    }

    /**
     * getQnaList by Product id
     *
     * @param productId productId
     * @return List<QnaboardDto>
     */
    @GetMapping("/qnas/{productId}")
    public List<QnaboardDto> getQnasByProductId(@PathVariable(name = "productId") int productId) {
//        ResponseEntity<List> response = restTemplate.exchange(url + ":" + port + "/qnas/" + productId, HttpMethod.GET, null, List.class);
        ResponseEntity<List> response = restTemplate.exchange(url + "/qnas/" + productId, HttpMethod.GET, null, List.class);
        return response.getBody();
    }

    /**
     * getQnaByid
     *
     * @param id id
     * @return QnaboardDto
     */
    @GetMapping("/qna/{id}")
    public QnaboardDto getQnaByid(@PathVariable(name = "id") int id) {
//        ResponseEntity<QnaboardDto> response = restTemplate.exchange(url + ":" + port + "/qna/" + id, HttpMethod.GET, null, QnaboardDto.class);
        ResponseEntity<QnaboardDto> response = restTemplate.exchange(url + "/qna/" + id, HttpMethod.GET, null, QnaboardDto.class);
        return response.getBody();
    }

    @PostMapping("/qna")
    public QnaboardDto qnaboard(
            @RequestHeader(value = "Authorization") String jwt,
            @RequestBody QnaboardForm qnaboardForm) {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwt);
        final HttpEntity<QnaboardForm> entity = new HttpEntity<>(qnaboardForm, headers);
//        ResponseEntity<QnaboardDto> response = restTemplate.exchange(url + ":" + port + "/qna", HttpMethod.POST, entity, QnaboardDto.class);
        ResponseEntity<QnaboardDto> response = restTemplate.exchange(url + "/qna", HttpMethod.POST, entity, QnaboardDto.class);
        return response.getBody();
    }

    /**
     * ログインする
     *
     * @param userLoginForm userLoginForm
     * @return jwt
     */
    @PostMapping("/login")
    public String login(@Valid @RequestBody UserLoginForm userLoginForm) {
        final HttpEntity<UserLoginForm> entity = new HttpEntity<>(userLoginForm);
//        ResponseEntity<String> response = restTemplate.exchange(url + ":" + port + "/login", HttpMethod.POST, entity, String.class);
        ResponseEntity<String> response = restTemplate.exchange(url + "/login", HttpMethod.POST, entity, String.class);
        return response.getBody();
    }

    /**
     * ユーザをロード
     *
     * @param jwt jwt
     * @return UserDto ユーザ情報
     */
    @PostMapping("/loadUser")
    public UserDto loadUser(@RequestHeader(value = "Authorization") String jwt) {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwt);
        final HttpEntity<HttpHeaders> entity = new HttpEntity<>(headers);
//        ResponseEntity<UserDto> response = restTemplate.exchange(url + ":" + port + "/loadUser", HttpMethod.POST, entity, UserDto.class);
        ResponseEntity<UserDto> response = restTemplate.exchange(url + "/loadUser", HttpMethod.POST, entity, UserDto.class);
        return response.getBody();
    }

    /**
     * ユーザ登録
     *
     * @param userRegisterForm userRegisterForm
     * @return success or Error
     */
    @PostMapping("/register")
    public String register(@Valid @RequestBody UserRegisterForm userRegisterForm) {
        final HttpEntity<UserRegisterForm> entity = new HttpEntity<>(userRegisterForm);
//        ResponseEntity<String> response = restTemplate.exchange(url + ":" + port + "/register", HttpMethod.POST, entity, String.class);
        ResponseEntity<String> response = restTemplate.exchange(url + "/register", HttpMethod.POST, entity, String.class);
        return response.getBody();
    }

    /**
     * get UserKubun
     *
     * @return List<UserKubun>
     */
    @GetMapping("/getUserKubun")
    public List<UserKubunDto> getUserKubun() {
//        ResponseEntity<List> response = restTemplate.exchange(url + ":" + port + "/getUserKubun", HttpMethod.GET, null, List.class);
        ResponseEntity<List> response = restTemplate.exchange(url + "/getUserKubun", HttpMethod.GET, null, List.class);
        return response.getBody();
    }

    /**
     * changeUserInfo
     *
     * @param userChangeInfoForm userChangeInfoForm
     * @param jwt                jwt
     * @return UserDto
     */
    @PostMapping("/changeUserInfo")
    public UserDto changeUserInfo(
            @RequestBody UserChangeInfoForm userChangeInfoForm,
            @RequestHeader(value = "Authorization") String jwt) {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwt);
        final HttpEntity<UserChangeInfoForm> entity = new HttpEntity<>(userChangeInfoForm, headers);
//        ResponseEntity<UserDto> response = restTemplate.exchange(url + ":" + port + "/changeUserInfo", HttpMethod.POST, entity, UserDto.class);
        ResponseEntity<UserDto> response = restTemplate.exchange(url + "/changeUserInfo", HttpMethod.POST, entity, UserDto.class);
        return response.getBody();
    }
}
